# Applications

## Branches

- **Development**: code management and branch release
- **Master**: 
    - release manager: 
        - tag/package/library: patch/fix, minor (X.Y.Z), major (X.Y)
        - container: build, latest, stable
    - deployment manager: 
        - target: on EC2 container and Lambda fct
        - environment: Development -> Integration -> Production

## Applications

### Core
#### <a href='https://gitlab.com/bo-art-of-bonsai/arti'><img src='images/arti.png' alt='arti' width='32px'/>arti</a>

- deploy_type: container
- editor: BO
- coverage: True
- description: Bonsai IA, provide understandable measures and advices
- technology: tensorflow, elasticsearch, fastapi, docker, python
- repository: https://gitlab.com/bo-art-of-bonsai/arti
- environment: development, integration, production
- security: OAuth2.0,ssl
- url: https://arti.bonsaibo.com
- doc: https://arti.bonsaibo.com/docs
 

| Delivery                    | Deployment                  |
| :-------------------------- | :-------------------------- |
| [![](https://gitlab.com/bo-art-of-bonsai/arti/badges/development/pipeline.svg)](https://gitlab.com/bo-art-of-bonsai/arti/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/arti/badges/master/pipeline.svg)](https://gitlab.com/bo-art-of-bonsai/arti/pipelines) |
| [![](https://gitlab.com/bo-art-of-bonsai/arti/-/jobs/artifacts/development/raw/version.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/arti/pipelines) | | [![](https://gitlab.com/bo-art-of-bonsai/arti/-/jobs/artifacts/master/raw/development.svg?job=deploy%20container%20on%20dev)](https://gitlab.com/bo-art-of-bonsai/arti/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/arti/-/jobs/artifacts/master/raw/integration.svg?job=deploy%20container%20on%20int)](https://gitlab.com/bo-art-of-bonsai/arti/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/arti/-/jobs/artifacts/master/raw/production.svg?job=deploy%20container%20on%20prod)](https://gitlab.com/bo-art-of-bonsai/arti/pipelines) |
| [![](https://gitlab.com/bo-art-of-bonsai/arti/-/jobs/artifacts/development/raw/python.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/arti) | [![](https://gitlab.com/bo-art-of-bonsai/arti/-/jobs/artifacts/master/raw/python.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/arti) |
| [![](https://gitlab.com/bo-art-of-bonsai/arti/-/jobs/artifacts/development/raw/bolibrary.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/arti) | [![](https://gitlab.com/bo-art-of-bonsai/arti/-/jobs/artifacts/master/raw/bolibrary.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/arti) |
| [![](https://gitlab.com/bo-art-of-bonsai/arti/-/jobs/artifacts/development/raw/commits.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/arti) | [![](https://gitlab.com/bo-art-of-bonsai/arti/-/jobs/artifacts/master/raw/commits.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/arti) |
| [![](https://gitlab.com/bo-art-of-bonsai/arti/-/jobs/artifacts/development/raw/last_commit.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/arti) | [![](https://gitlab.com/bo-art-of-bonsai/arti/-/jobs/artifacts/master/raw/last_commit.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/arti) |
| [![](https://gitlab.com/bo-art-of-bonsai/arti/-/jobs/artifacts/development/raw/pylint.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/arti/-/jobs) | |
| [![](https://gitlab.com/bo-art-of-bonsai/arti/badges/development/coverage.svg)](https://gitlab.com/bo-art-of-bonsai/arti/-/jobs) | |
 

#### <a href='https://gitlab.com/bo-art-of-bonsai/blocker'><img src='images/blocker.png' alt='blocker' width='32px'/>blocker</a>

- deploy_type: container
- editor: BO
- coverage: True
- description: Blockchain solution to stamp IoTree reports
- technology: infura, ethereum, elasticsearch, s3, fastapi, python, docker
- repository: https://gitlab.com/bo-art-of-bonsai/blocker
- environment: development, integration, production
- security: OAuth2.0,ssl
- url: https://blocker.bonsaibo.com
- doc: https://blocker.bonsaibo.com/docs
 

| Delivery                    | Deployment                  |
| :-------------------------- | :-------------------------- |
| [![](https://gitlab.com/bo-art-of-bonsai/blocker/badges/development/pipeline.svg)](https://gitlab.com/bo-art-of-bonsai/blocker/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/blocker/badges/master/pipeline.svg)](https://gitlab.com/bo-art-of-bonsai/blocker/pipelines) |
| [![](https://gitlab.com/bo-art-of-bonsai/blocker/-/jobs/artifacts/development/raw/version.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/blocker/pipelines) | | [![](https://gitlab.com/bo-art-of-bonsai/blocker/-/jobs/artifacts/master/raw/development.svg?job=deploy%20container%20on%20dev)](https://gitlab.com/bo-art-of-bonsai/blocker/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/blocker/-/jobs/artifacts/master/raw/integration.svg?job=deploy%20container%20on%20int)](https://gitlab.com/bo-art-of-bonsai/blocker/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/blocker/-/jobs/artifacts/master/raw/production.svg?job=deploy%20container%20on%20prod)](https://gitlab.com/bo-art-of-bonsai/blocker/pipelines) |
| [![](https://gitlab.com/bo-art-of-bonsai/blocker/-/jobs/artifacts/development/raw/python.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/blocker) | [![](https://gitlab.com/bo-art-of-bonsai/blocker/-/jobs/artifacts/master/raw/python.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/blocker) |
| [![](https://gitlab.com/bo-art-of-bonsai/blocker/-/jobs/artifacts/development/raw/bolibrary.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/blocker) | [![](https://gitlab.com/bo-art-of-bonsai/blocker/-/jobs/artifacts/master/raw/bolibrary.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/blocker) |
| [![](https://gitlab.com/bo-art-of-bonsai/blocker/-/jobs/artifacts/development/raw/commits.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/blocker) | [![](https://gitlab.com/bo-art-of-bonsai/blocker/-/jobs/artifacts/master/raw/commits.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/blocker) |
| [![](https://gitlab.com/bo-art-of-bonsai/blocker/-/jobs/artifacts/development/raw/last_commit.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/blocker) | [![](https://gitlab.com/bo-art-of-bonsai/blocker/-/jobs/artifacts/master/raw/last_commit.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/blocker) |
| [![](https://gitlab.com/bo-art-of-bonsai/blocker/-/jobs/artifacts/development/raw/pylint.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/blocker/-/jobs) | |
| [![](https://gitlab.com/bo-art-of-bonsai/blocker/badges/development/coverage.svg)](https://gitlab.com/bo-art-of-bonsai/blocker/-/jobs) | |
 

#### <a href='https://gitlab.com/bo-art-of-bonsai/collector'><img src='images/collector.png' alt='collector' width='32px'/>collector</a>

- deploy_type: serverless
- editor: BO
- coverage: True
- description: Collect and index IoT data
- technology: lambda, elasticsearch, python
- repository: https://gitlab.com/bo-art-of-bonsai/collector
- environment: development, integration, production
- security: AWS
- url: 
- doc: 
 

| Delivery                    | Deployment                  |
| :-------------------------- | :-------------------------- |
| [![](https://gitlab.com/bo-art-of-bonsai/collector/badges/development/pipeline.svg)](https://gitlab.com/bo-art-of-bonsai/collector/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/collector/badges/master/pipeline.svg)](https://gitlab.com/bo-art-of-bonsai/collector/pipelines) |
| [![](https://gitlab.com/bo-art-of-bonsai/collector/-/jobs/artifacts/development/raw/version.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/collector/pipelines) | | [![](https://gitlab.com/bo-art-of-bonsai/collector/-/jobs/artifacts/master/raw/development.svg?job=deploy%20serverless%20on%20dev)](https://gitlab.com/bo-art-of-bonsai/collector/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/collector/-/jobs/artifacts/master/raw/integration.svg?job=deploy%20serverless%20on%20int)](https://gitlab.com/bo-art-of-bonsai/collector/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/collector/-/jobs/artifacts/master/raw/production.svg?job=deploy%20serverless%20on%20prod)](https://gitlab.com/bo-art-of-bonsai/collector/pipelines) |
| [![](https://gitlab.com/bo-art-of-bonsai/collector/-/jobs/artifacts/development/raw/python.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/collector) | [![](https://gitlab.com/bo-art-of-bonsai/collector/-/jobs/artifacts/master/raw/python.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/collector) |
| [![](https://gitlab.com/bo-art-of-bonsai/collector/-/jobs/artifacts/development/raw/bolibrary.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/collector) | [![](https://gitlab.com/bo-art-of-bonsai/collector/-/jobs/artifacts/master/raw/bolibrary.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/collector) |
| [![](https://gitlab.com/bo-art-of-bonsai/collector/-/jobs/artifacts/development/raw/commits.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/collector) | [![](https://gitlab.com/bo-art-of-bonsai/collector/-/jobs/artifacts/master/raw/commits.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/collector) |
| [![](https://gitlab.com/bo-art-of-bonsai/collector/-/jobs/artifacts/development/raw/last_commit.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/collector) | [![](https://gitlab.com/bo-art-of-bonsai/collector/-/jobs/artifacts/master/raw/last_commit.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/collector) |
| [![](https://gitlab.com/bo-art-of-bonsai/collector/-/jobs/artifacts/development/raw/pylint.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/collector/-/jobs) | |
| [![](https://gitlab.com/bo-art-of-bonsai/collector/badges/development/coverage.svg)](https://gitlab.com/bo-art-of-bonsai/collector/-/jobs) | |
 

#### <a href='https://gitlab.com/bo-art-of-bonsai/forecast'><img src='images/forecast.png' alt='forecast' width='32px'/>forecast</a>

- deploy_type: serverless
- editor: BO
- coverage: True
- description: Collect and index weather forecast
- technology: lambda, openweathermap, directus, elasticsearch
- repository: https://gitlab.com/bo-art-of-bonsai/forecast
- environment: development, integration, production
- security: AWS
- url: 
- doc: 
 

| Delivery                    | Deployment                  |
| :-------------------------- | :-------------------------- |
| [![](https://gitlab.com/bo-art-of-bonsai/forecast/badges/development/pipeline.svg)](https://gitlab.com/bo-art-of-bonsai/forecast/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/forecast/badges/master/pipeline.svg)](https://gitlab.com/bo-art-of-bonsai/forecast/pipelines) |
| [![](https://gitlab.com/bo-art-of-bonsai/forecast/-/jobs/artifacts/development/raw/version.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/forecast/pipelines) | | [![](https://gitlab.com/bo-art-of-bonsai/forecast/-/jobs/artifacts/master/raw/development.svg?job=deploy%20serverless%20on%20dev)](https://gitlab.com/bo-art-of-bonsai/forecast/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/forecast/-/jobs/artifacts/master/raw/integration.svg?job=deploy%20serverless%20on%20int)](https://gitlab.com/bo-art-of-bonsai/forecast/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/forecast/-/jobs/artifacts/master/raw/production.svg?job=deploy%20serverless%20on%20prod)](https://gitlab.com/bo-art-of-bonsai/forecast/pipelines) |
| [![](https://gitlab.com/bo-art-of-bonsai/forecast/-/jobs/artifacts/development/raw/python.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/forecast) | [![](https://gitlab.com/bo-art-of-bonsai/forecast/-/jobs/artifacts/master/raw/python.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/forecast) |
| [![](https://gitlab.com/bo-art-of-bonsai/forecast/-/jobs/artifacts/development/raw/bolibrary.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/forecast) | [![](https://gitlab.com/bo-art-of-bonsai/forecast/-/jobs/artifacts/master/raw/bolibrary.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/forecast) |
| [![](https://gitlab.com/bo-art-of-bonsai/forecast/-/jobs/artifacts/development/raw/commits.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/forecast) | [![](https://gitlab.com/bo-art-of-bonsai/forecast/-/jobs/artifacts/master/raw/commits.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/forecast) |
| [![](https://gitlab.com/bo-art-of-bonsai/forecast/-/jobs/artifacts/development/raw/last_commit.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/forecast) | [![](https://gitlab.com/bo-art-of-bonsai/forecast/-/jobs/artifacts/master/raw/last_commit.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/forecast) |
| [![](https://gitlab.com/bo-art-of-bonsai/forecast/-/jobs/artifacts/development/raw/pylint.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/forecast/-/jobs) | |
| [![](https://gitlab.com/bo-art-of-bonsai/forecast/badges/development/coverage.svg)](https://gitlab.com/bo-art-of-bonsai/forecast/-/jobs) | |
 

#### <a href='https://gitlab.com/bo-art-of-bonsai/image-analyzer'><img src='images/image-analyzer.png' alt='image-analyzer' width='32px'/>image-analyzer</a>

- deploy_type: serverless
- editor: BO
- coverage: True
- description: Image tools to search, analyse, train and recognize 
- technology: tensorflow, opencv, lambda, s3, python
- repository: https://gitlab.com/bo-art-of-bonsai/image-analyzer
- environment: development, integration, production
- security: AWS
- url: 
- doc: 
 

| Delivery                    | Deployment                  |
| :-------------------------- | :-------------------------- |
| [![](https://gitlab.com/bo-art-of-bonsai/image-analyzer/badges/development/pipeline.svg)](https://gitlab.com/bo-art-of-bonsai/image-analyzer/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/image-analyzer/badges/master/pipeline.svg)](https://gitlab.com/bo-art-of-bonsai/image-analyzer/pipelines) |
| [![](https://gitlab.com/bo-art-of-bonsai/image-analyzer/-/jobs/artifacts/development/raw/version.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/image-analyzer/pipelines) | | [![](https://gitlab.com/bo-art-of-bonsai/image-analyzer/-/jobs/artifacts/master/raw/development.svg?job=deploy%20serverless%20on%20dev)](https://gitlab.com/bo-art-of-bonsai/image-analyzer/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/image-analyzer/-/jobs/artifacts/master/raw/integration.svg?job=deploy%20serverless%20on%20int)](https://gitlab.com/bo-art-of-bonsai/image-analyzer/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/image-analyzer/-/jobs/artifacts/master/raw/production.svg?job=deploy%20serverless%20on%20prod)](https://gitlab.com/bo-art-of-bonsai/image-analyzer/pipelines) |
| [![](https://gitlab.com/bo-art-of-bonsai/image-analyzer/-/jobs/artifacts/development/raw/python.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/image-analyzer) | [![](https://gitlab.com/bo-art-of-bonsai/image-analyzer/-/jobs/artifacts/master/raw/python.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/image-analyzer) |
| [![](https://gitlab.com/bo-art-of-bonsai/image-analyzer/-/jobs/artifacts/development/raw/bolibrary.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/image-analyzer) | [![](https://gitlab.com/bo-art-of-bonsai/image-analyzer/-/jobs/artifacts/master/raw/bolibrary.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/image-analyzer) |
| [![](https://gitlab.com/bo-art-of-bonsai/image-analyzer/-/jobs/artifacts/development/raw/commits.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/image-analyzer) | [![](https://gitlab.com/bo-art-of-bonsai/image-analyzer/-/jobs/artifacts/master/raw/commits.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/image-analyzer) |
| [![](https://gitlab.com/bo-art-of-bonsai/image-analyzer/-/jobs/artifacts/development/raw/last_commit.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/image-analyzer) | [![](https://gitlab.com/bo-art-of-bonsai/image-analyzer/-/jobs/artifacts/master/raw/last_commit.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/image-analyzer) |
| [![](https://gitlab.com/bo-art-of-bonsai/image-analyzer/-/jobs/artifacts/development/raw/pylint.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/image-analyzer/-/jobs) | |
| [![](https://gitlab.com/bo-art-of-bonsai/image-analyzer/badges/development/coverage.svg)](https://gitlab.com/bo-art-of-bonsai/image-analyzer/-/jobs) | |
 

#### <a href='https://gitlab.com/bo-art-of-bonsai/notifier'><img src='images/notifier.png' alt='notifier' width='32px'/>notifier</a>

- deploy_type: serverless
- editor: BO
- coverage: True
- description: Notification on SMS or WhatsApp triggered by Elsatic watcher
- technology: twilio, lambda, elastic, python
- repository: https://gitlab.com/bo-art-of-bonsai/notifier
- environment: development, integration, production
- security: AWS
- url: 
- doc: 
 

| Delivery                    | Deployment                  |
| :-------------------------- | :-------------------------- |
| [![](https://gitlab.com/bo-art-of-bonsai/notifier/badges/development/pipeline.svg)](https://gitlab.com/bo-art-of-bonsai/notifier/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/notifier/badges/master/pipeline.svg)](https://gitlab.com/bo-art-of-bonsai/notifier/pipelines) |
| [![](https://gitlab.com/bo-art-of-bonsai/notifier/-/jobs/artifacts/development/raw/version.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/notifier/pipelines) | | [![](https://gitlab.com/bo-art-of-bonsai/notifier/-/jobs/artifacts/master/raw/development.svg?job=deploy%20serverless%20on%20dev)](https://gitlab.com/bo-art-of-bonsai/notifier/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/notifier/-/jobs/artifacts/master/raw/integration.svg?job=deploy%20serverless%20on%20int)](https://gitlab.com/bo-art-of-bonsai/notifier/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/notifier/-/jobs/artifacts/master/raw/production.svg?job=deploy%20serverless%20on%20prod)](https://gitlab.com/bo-art-of-bonsai/notifier/pipelines) |
| [![](https://gitlab.com/bo-art-of-bonsai/notifier/-/jobs/artifacts/development/raw/python.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/notifier) | [![](https://gitlab.com/bo-art-of-bonsai/notifier/-/jobs/artifacts/master/raw/python.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/notifier) |
| [![](https://gitlab.com/bo-art-of-bonsai/notifier/-/jobs/artifacts/development/raw/bolibrary.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/notifier) | [![](https://gitlab.com/bo-art-of-bonsai/notifier/-/jobs/artifacts/master/raw/bolibrary.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/notifier) |
| [![](https://gitlab.com/bo-art-of-bonsai/notifier/-/jobs/artifacts/development/raw/commits.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/notifier) | [![](https://gitlab.com/bo-art-of-bonsai/notifier/-/jobs/artifacts/master/raw/commits.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/notifier) |
| [![](https://gitlab.com/bo-art-of-bonsai/notifier/-/jobs/artifacts/development/raw/last_commit.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/notifier) | [![](https://gitlab.com/bo-art-of-bonsai/notifier/-/jobs/artifacts/master/raw/last_commit.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/notifier) |
| [![](https://gitlab.com/bo-art-of-bonsai/notifier/-/jobs/artifacts/development/raw/pylint.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/notifier/-/jobs) | |
| [![](https://gitlab.com/bo-art-of-bonsai/notifier/badges/development/coverage.svg)](https://gitlab.com/bo-art-of-bonsai/notifier/-/jobs) | |
 

#### <a href='https://gitlab.com/bo-art-of-bonsai/provisioning'><img src='images/provisioning.png' alt='provisioning' width='32px'/>provisioning</a>

- deploy_type: container
- editor: directus
- coverage: False
- description: Provide CMDB feature and user management
- technology: directus, php, laravel, mysql, docker
- repository: https://gitlab.com/bo-art-of-bonsai/provisioning
- environment: development, integration, production
- security: MFA,OAuth2.0,ssl
- url: https://provisioning.bonsaibo.com
- doc: 
 

| Delivery                    | Deployment                  |
| :-------------------------- | :-------------------------- |
| [![](https://gitlab.com/bo-art-of-bonsai/provisioning/badges/development/pipeline.svg)](https://gitlab.com/bo-art-of-bonsai/provisioning/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/provisioning/badges/master/pipeline.svg)](https://gitlab.com/bo-art-of-bonsai/provisioning/pipelines) |
| [![](https://gitlab.com/bo-art-of-bonsai/provisioning/-/jobs/artifacts/development/raw/version.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/provisioning/pipelines) | | [![](https://gitlab.com/bo-art-of-bonsai/provisioning/-/jobs/artifacts/master/raw/development.svg?job=deploy%20container%20on%20dev)](https://gitlab.com/bo-art-of-bonsai/provisioning/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/provisioning/-/jobs/artifacts/master/raw/integration.svg?job=deploy%20container%20on%20int)](https://gitlab.com/bo-art-of-bonsai/provisioning/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/provisioning/-/jobs/artifacts/master/raw/production.svg?job=deploy%20container%20on%20prod)](https://gitlab.com/bo-art-of-bonsai/provisioning/pipelines) |
| [![](https://gitlab.com/bo-art-of-bonsai/provisioning/-/jobs/artifacts/development/raw/python.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/provisioning) | [![](https://gitlab.com/bo-art-of-bonsai/provisioning/-/jobs/artifacts/master/raw/python.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/provisioning) |
| [![](https://gitlab.com/bo-art-of-bonsai/provisioning/-/jobs/artifacts/development/raw/bolibrary.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/provisioning) | [![](https://gitlab.com/bo-art-of-bonsai/provisioning/-/jobs/artifacts/master/raw/bolibrary.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/provisioning) |
| [![](https://gitlab.com/bo-art-of-bonsai/provisioning/-/jobs/artifacts/development/raw/commits.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/provisioning) | [![](https://gitlab.com/bo-art-of-bonsai/provisioning/-/jobs/artifacts/master/raw/commits.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/provisioning) |
| [![](https://gitlab.com/bo-art-of-bonsai/provisioning/-/jobs/artifacts/development/raw/last_commit.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/provisioning) | [![](https://gitlab.com/bo-art-of-bonsai/provisioning/-/jobs/artifacts/master/raw/last_commit.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/provisioning) |
| [![](https://gitlab.com/bo-art-of-bonsai/provisioning/-/jobs/artifacts/development/raw/pylint.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/provisioning/-/jobs) | |
| [![](https://gitlab.com/bo-art-of-bonsai/provisioning/badges/development/coverage.svg)](https://gitlab.com/bo-art-of-bonsai/provisioning/-/jobs) | |
 

#### <a href='https://gitlab.com/bo-art-of-bonsai/terms'><img src='images/terms.png' alt='terms' width='32px'/>terms</a>

- deploy_type: container
- editor: BO
- coverage: False
- description: HTML static files for the Terms needs
- technology: html, docker
- repository: https://gitlab.com/bo-art-of-bonsai/terms
- environment: development, integration, production
- security: no
- url: https://www.bonsaibo.com
- doc: 
 

| Delivery                    | Deployment                  |
| :-------------------------- | :-------------------------- |
| [![](https://gitlab.com/bo-art-of-bonsai/terms/badges/development/pipeline.svg)](https://gitlab.com/bo-art-of-bonsai/terms/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/terms/badges/master/pipeline.svg)](https://gitlab.com/bo-art-of-bonsai/terms/pipelines) |
| [![](https://gitlab.com/bo-art-of-bonsai/terms/-/jobs/artifacts/development/raw/version.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/terms/pipelines) | | [![](https://gitlab.com/bo-art-of-bonsai/terms/-/jobs/artifacts/master/raw/development.svg?job=deploy%20container%20on%20dev)](https://gitlab.com/bo-art-of-bonsai/terms/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/terms/-/jobs/artifacts/master/raw/integration.svg?job=deploy%20container%20on%20int)](https://gitlab.com/bo-art-of-bonsai/terms/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/terms/-/jobs/artifacts/master/raw/production.svg?job=deploy%20container%20on%20prod)](https://gitlab.com/bo-art-of-bonsai/terms/pipelines) |
| [![](https://gitlab.com/bo-art-of-bonsai/terms/-/jobs/artifacts/development/raw/python.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/terms) | [![](https://gitlab.com/bo-art-of-bonsai/terms/-/jobs/artifacts/master/raw/python.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/terms) |
| [![](https://gitlab.com/bo-art-of-bonsai/terms/-/jobs/artifacts/development/raw/bolibrary.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/terms) | [![](https://gitlab.com/bo-art-of-bonsai/terms/-/jobs/artifacts/master/raw/bolibrary.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/terms) |
| [![](https://gitlab.com/bo-art-of-bonsai/terms/-/jobs/artifacts/development/raw/commits.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/terms) | [![](https://gitlab.com/bo-art-of-bonsai/terms/-/jobs/artifacts/master/raw/commits.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/terms) |
| [![](https://gitlab.com/bo-art-of-bonsai/terms/-/jobs/artifacts/development/raw/last_commit.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/terms) | [![](https://gitlab.com/bo-art-of-bonsai/terms/-/jobs/artifacts/master/raw/last_commit.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/terms) |
| [![](https://gitlab.com/bo-art-of-bonsai/terms/-/jobs/artifacts/development/raw/pylint.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/terms/-/jobs) | |
| [![](https://gitlab.com/bo-art-of-bonsai/terms/badges/development/coverage.svg)](https://gitlab.com/bo-art-of-bonsai/terms/-/jobs) | |
 

#### <a href='https://gitlab.com/bo-art-of-bonsai/www'><img src='images/www.png' alt='www' width='32px'/>www</a>

- deploy_type: container
- editor: BO
- coverage: False
- description: HTML static page as greeting
- technology: html, docker
- repository: https://gitlab.com/bo-art-of-bonsai/www
- environment: development, integration, production
- security: no
- url: https://www.bonsaibo.com
- doc: 
 

| Delivery                    | Deployment                  |
| :-------------------------- | :-------------------------- |
| [![](https://gitlab.com/bo-art-of-bonsai/www/badges/development/pipeline.svg)](https://gitlab.com/bo-art-of-bonsai/www/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/www/badges/master/pipeline.svg)](https://gitlab.com/bo-art-of-bonsai/www/pipelines) |
| [![](https://gitlab.com/bo-art-of-bonsai/www/-/jobs/artifacts/development/raw/version.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/www/pipelines) | | [![](https://gitlab.com/bo-art-of-bonsai/www/-/jobs/artifacts/master/raw/development.svg?job=deploy%20container%20on%20dev)](https://gitlab.com/bo-art-of-bonsai/www/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/www/-/jobs/artifacts/master/raw/integration.svg?job=deploy%20container%20on%20int)](https://gitlab.com/bo-art-of-bonsai/www/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/www/-/jobs/artifacts/master/raw/production.svg?job=deploy%20container%20on%20prod)](https://gitlab.com/bo-art-of-bonsai/www/pipelines) |
| [![](https://gitlab.com/bo-art-of-bonsai/www/-/jobs/artifacts/development/raw/python.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/www) | [![](https://gitlab.com/bo-art-of-bonsai/www/-/jobs/artifacts/master/raw/python.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/www) |
| [![](https://gitlab.com/bo-art-of-bonsai/www/-/jobs/artifacts/development/raw/bolibrary.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/www) | [![](https://gitlab.com/bo-art-of-bonsai/www/-/jobs/artifacts/master/raw/bolibrary.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/www) |
| [![](https://gitlab.com/bo-art-of-bonsai/www/-/jobs/artifacts/development/raw/commits.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/www) | [![](https://gitlab.com/bo-art-of-bonsai/www/-/jobs/artifacts/master/raw/commits.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/www) |
| [![](https://gitlab.com/bo-art-of-bonsai/www/-/jobs/artifacts/development/raw/last_commit.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/www) | [![](https://gitlab.com/bo-art-of-bonsai/www/-/jobs/artifacts/master/raw/last_commit.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/www) |
| [![](https://gitlab.com/bo-art-of-bonsai/www/-/jobs/artifacts/development/raw/pylint.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/www/-/jobs) | |
| [![](https://gitlab.com/bo-art-of-bonsai/www/badges/development/coverage.svg)](https://gitlab.com/bo-art-of-bonsai/www/-/jobs) | |
 

### Frameworks
#### <a href='https://gitlab.com/bo-art-of-bonsai/library'><img src='images/library.png' alt='library' width='32px'/>library</a>

- deploy_type: library
- editor: BO
- coverage: True
- description: Centralized library used to provide generic Class and configuration
- technology: python, pydantic
- repository: https://gitlab.com/bo-art-of-bonsai/library
- environment: development, integration, production
- security: no
- url: 
- doc: 
 

| Delivery                    | Deployment                  |
| :-------------------------- | :-------------------------- |
| [![](https://gitlab.com/bo-art-of-bonsai/library/badges/development/pipeline.svg)](https://gitlab.com/bo-art-of-bonsai/library/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/library/badges/master/pipeline.svg)](https://gitlab.com/bo-art-of-bonsai/library/pipelines) |
| [![](https://gitlab.com/bo-art-of-bonsai/library/-/jobs/artifacts/development/raw/version.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/library/pipelines) | | [![](https://gitlab.com/bo-art-of-bonsai/library/-/jobs/artifacts/master/raw/development.svg?job=deploy%20library%20on%20dev)](https://gitlab.com/bo-art-of-bonsai/library/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/library/-/jobs/artifacts/master/raw/integration.svg?job=deploy%20library%20on%20int)](https://gitlab.com/bo-art-of-bonsai/library/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/library/-/jobs/artifacts/master/raw/production.svg?job=deploy%20library%20on%20prod)](https://gitlab.com/bo-art-of-bonsai/library/pipelines) |
| [![](https://gitlab.com/bo-art-of-bonsai/library/-/jobs/artifacts/development/raw/python.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/library) | [![](https://gitlab.com/bo-art-of-bonsai/library/-/jobs/artifacts/master/raw/python.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/library) |
| [![](https://gitlab.com/bo-art-of-bonsai/library/-/jobs/artifacts/development/raw/bolibrary.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/library) | [![](https://gitlab.com/bo-art-of-bonsai/library/-/jobs/artifacts/master/raw/bolibrary.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/library) |
| [![](https://gitlab.com/bo-art-of-bonsai/library/-/jobs/artifacts/development/raw/commits.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/library) | [![](https://gitlab.com/bo-art-of-bonsai/library/-/jobs/artifacts/master/raw/commits.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/library) |
| [![](https://gitlab.com/bo-art-of-bonsai/library/-/jobs/artifacts/development/raw/last_commit.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/library) | [![](https://gitlab.com/bo-art-of-bonsai/library/-/jobs/artifacts/master/raw/last_commit.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/library) |
| [![](https://gitlab.com/bo-art-of-bonsai/library/-/jobs/artifacts/development/raw/pylint.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/library/-/jobs) | |
| [![](https://gitlab.com/bo-art-of-bonsai/library/badges/development/coverage.svg)](https://gitlab.com/bo-art-of-bonsai/library/-/jobs) | |
 

#### <a href='https://gitlab.com/bo-art-of-bonsai/infra'><img src='images/infra.png' alt='infra' width='32px'/>infra</a>

- deploy_type: 
- editor: BO
- coverage: False
- description: IT micro-services management
- technology: docker, docker-compose, gitlab,registry
- repository: https://gitlab.com/bo-art-of-bonsai/infra
- environment: development, integration, production
- security: docker,ec2
- url: 
- doc: 
 

| Delivery                    | Deployment                  |
| :-------------------------- | :-------------------------- |
| [![](https://gitlab.com/bo-art-of-bonsai/infra/badges/development/pipeline.svg)](https://gitlab.com/bo-art-of-bonsai/infra/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/infra/badges/master/pipeline.svg)](https://gitlab.com/bo-art-of-bonsai/infra/pipelines) |
| [![](https://gitlab.com/bo-art-of-bonsai/infra/-/jobs/artifacts/development/raw/version.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/infra/pipelines) | | [![](https://gitlab.com/bo-art-of-bonsai/infra/-/jobs/artifacts/master/raw/development.svg?job=deploy%20%20on%20dev)](https://gitlab.com/bo-art-of-bonsai/infra/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/infra/-/jobs/artifacts/master/raw/integration.svg?job=deploy%20%20on%20int)](https://gitlab.com/bo-art-of-bonsai/infra/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/infra/-/jobs/artifacts/master/raw/production.svg?job=deploy%20%20on%20prod)](https://gitlab.com/bo-art-of-bonsai/infra/pipelines) |
| [![](https://gitlab.com/bo-art-of-bonsai/infra/-/jobs/artifacts/development/raw/python.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/infra) | [![](https://gitlab.com/bo-art-of-bonsai/infra/-/jobs/artifacts/master/raw/python.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/infra) |
| [![](https://gitlab.com/bo-art-of-bonsai/infra/-/jobs/artifacts/development/raw/bolibrary.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/infra) | [![](https://gitlab.com/bo-art-of-bonsai/infra/-/jobs/artifacts/master/raw/bolibrary.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/infra) |
| [![](https://gitlab.com/bo-art-of-bonsai/infra/-/jobs/artifacts/development/raw/commits.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/infra) | [![](https://gitlab.com/bo-art-of-bonsai/infra/-/jobs/artifacts/master/raw/commits.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/infra) |
| [![](https://gitlab.com/bo-art-of-bonsai/infra/-/jobs/artifacts/development/raw/last_commit.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/infra) | [![](https://gitlab.com/bo-art-of-bonsai/infra/-/jobs/artifacts/master/raw/last_commit.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/infra) |
| [![](https://gitlab.com/bo-art-of-bonsai/infra/-/jobs/artifacts/development/raw/pylint.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/infra/-/jobs) | |
| [![](https://gitlab.com/bo-art-of-bonsai/infra/badges/development/coverage.svg)](https://gitlab.com/bo-art-of-bonsai/infra/-/jobs) | |
 

#### <a href='https://gitlab.com/bo-art-of-bonsai/snitch'><img src='images/snitch.png' alt='snitch' width='32px'/>snitch</a>

- deploy_type: 
- editor: BO
- coverage: True
- description: Doc, report and dashboard
- technology: markdown, uml, json, png, python
- repository: https://gitlab.com/bo-art-of-bonsai/snitch
- environment: development, backend
- security: gitlab
- url: 
- doc: 
 

| Delivery                    | Deployment                  |
| :-------------------------- | :-------------------------- |
| [![](https://gitlab.com/bo-art-of-bonsai/snitch/badges/development/pipeline.svg)](https://gitlab.com/bo-art-of-bonsai/snitch/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/snitch/badges/master/pipeline.svg)](https://gitlab.com/bo-art-of-bonsai/snitch/pipelines) |
| [![](https://gitlab.com/bo-art-of-bonsai/snitch/-/jobs/artifacts/development/raw/version.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/snitch/pipelines) | | [![](https://gitlab.com/bo-art-of-bonsai/snitch/-/jobs/artifacts/master/raw/development.svg?job=deploy%20%20on%20dev)](https://gitlab.com/bo-art-of-bonsai/snitch/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/snitch/-/jobs/artifacts/master/raw/integration.svg?job=deploy%20%20on%20int)](https://gitlab.com/bo-art-of-bonsai/snitch/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/snitch/-/jobs/artifacts/master/raw/production.svg?job=deploy%20%20on%20prod)](https://gitlab.com/bo-art-of-bonsai/snitch/pipelines) |
| [![](https://gitlab.com/bo-art-of-bonsai/snitch/-/jobs/artifacts/development/raw/python.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/snitch) | [![](https://gitlab.com/bo-art-of-bonsai/snitch/-/jobs/artifacts/master/raw/python.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/snitch) |
| [![](https://gitlab.com/bo-art-of-bonsai/snitch/-/jobs/artifacts/development/raw/bolibrary.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/snitch) | [![](https://gitlab.com/bo-art-of-bonsai/snitch/-/jobs/artifacts/master/raw/bolibrary.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/snitch) |
| [![](https://gitlab.com/bo-art-of-bonsai/snitch/-/jobs/artifacts/development/raw/commits.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/snitch) | [![](https://gitlab.com/bo-art-of-bonsai/snitch/-/jobs/artifacts/master/raw/commits.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/snitch) |
| [![](https://gitlab.com/bo-art-of-bonsai/snitch/-/jobs/artifacts/development/raw/last_commit.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/snitch) | [![](https://gitlab.com/bo-art-of-bonsai/snitch/-/jobs/artifacts/master/raw/last_commit.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/snitch) |
| [![](https://gitlab.com/bo-art-of-bonsai/snitch/-/jobs/artifacts/development/raw/pylint.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/snitch/-/jobs) | |
| [![](https://gitlab.com/bo-art-of-bonsai/snitch/badges/development/coverage.svg)](https://gitlab.com/bo-art-of-bonsai/snitch/-/jobs) | |
 

### Automates
#### <a href='https://gitlab.com/bo-art-of-bonsai/automates/cloudwatch'><img src='images/cloudwatch.png' alt='cloudwatch' width='32px'/>cloudwatch</a>

- deploy_type: serverless
- editor: BO
- coverage: True
- description: Collect, filter, format and index CloudWatch alerts in Elasticsearch
- technology: cloudwatch, lambda, elasticsearch, python
- repository: https://gitlab.com/bo-art-of-bonsai/automates/cloudwatch
- environment: development, integration, production
- security: aws
- url: 
- doc: 
 

| Delivery                    | Deployment                  |
| :-------------------------- | :-------------------------- |
| [![](https://gitlab.com/bo-art-of-bonsai/automates/cloudwatch/badges/development/pipeline.svg)](https://gitlab.com/bo-art-of-bonsai/automates/cloudwatch/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/automates/cloudwatch/badges/master/pipeline.svg)](https://gitlab.com/bo-art-of-bonsai/automates/cloudwatch/pipelines) |
| [![](https://gitlab.com/bo-art-of-bonsai/automates/cloudwatch/-/jobs/artifacts/development/raw/version.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/cloudwatch/pipelines) | | [![](https://gitlab.com/bo-art-of-bonsai/automates/cloudwatch/-/jobs/artifacts/master/raw/development.svg?job=deploy%20serverless%20on%20dev)](https://gitlab.com/bo-art-of-bonsai/automates/cloudwatch/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/automates/cloudwatch/-/jobs/artifacts/master/raw/integration.svg?job=deploy%20serverless%20on%20int)](https://gitlab.com/bo-art-of-bonsai/automates/cloudwatch/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/automates/cloudwatch/-/jobs/artifacts/master/raw/production.svg?job=deploy%20serverless%20on%20prod)](https://gitlab.com/bo-art-of-bonsai/automates/cloudwatch/pipelines) |
| [![](https://gitlab.com/bo-art-of-bonsai/automates/cloudwatch/-/jobs/artifacts/development/raw/python.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/cloudwatch) | [![](https://gitlab.com/bo-art-of-bonsai/automates/cloudwatch/-/jobs/artifacts/master/raw/python.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/cloudwatch) |
| [![](https://gitlab.com/bo-art-of-bonsai/automates/cloudwatch/-/jobs/artifacts/development/raw/bolibrary.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/cloudwatch) | [![](https://gitlab.com/bo-art-of-bonsai/automates/cloudwatch/-/jobs/artifacts/master/raw/bolibrary.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/cloudwatch) |
| [![](https://gitlab.com/bo-art-of-bonsai/automates/cloudwatch/-/jobs/artifacts/development/raw/commits.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/cloudwatch) | [![](https://gitlab.com/bo-art-of-bonsai/automates/cloudwatch/-/jobs/artifacts/master/raw/commits.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/cloudwatch) |
| [![](https://gitlab.com/bo-art-of-bonsai/automates/cloudwatch/-/jobs/artifacts/development/raw/last_commit.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/cloudwatch) | [![](https://gitlab.com/bo-art-of-bonsai/automates/cloudwatch/-/jobs/artifacts/master/raw/last_commit.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/cloudwatch) |
| [![](https://gitlab.com/bo-art-of-bonsai/automates/cloudwatch/-/jobs/artifacts/development/raw/pylint.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/cloudwatch/-/jobs) | |
| [![](https://gitlab.com/bo-art-of-bonsai/automates/cloudwatch/badges/development/coverage.svg)](https://gitlab.com/bo-art-of-bonsai/automates/cloudwatch/-/jobs) | |
 

#### <a href='https://gitlab.com/bo-art-of-bonsai/automates/watcher'><img src='images/watcher.png' alt='watcher' width='32px'/>watcher</a>

- deploy_type: container
- editor: BO
- coverage: True
- description: Generate the watchers from templates and publish them in Elasticsearch 
- technology: elasticsearch, python, json
- repository: https://gitlab.com/bo-art-of-bonsai/automates/watcher
- environment: development, backend
- security: no
- url: 
- doc: 
 

| Delivery                    | Deployment                  |
| :-------------------------- | :-------------------------- |
| [![](https://gitlab.com/bo-art-of-bonsai/automates/watcher/badges/development/pipeline.svg)](https://gitlab.com/bo-art-of-bonsai/automates/watcher/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/automates/watcher/badges/master/pipeline.svg)](https://gitlab.com/bo-art-of-bonsai/automates/watcher/pipelines) |
| [![](https://gitlab.com/bo-art-of-bonsai/automates/watcher/-/jobs/artifacts/development/raw/version.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/watcher/pipelines) | | [![](https://gitlab.com/bo-art-of-bonsai/automates/watcher/-/jobs/artifacts/master/raw/development.svg?job=deploy%20container%20on%20dev)](https://gitlab.com/bo-art-of-bonsai/automates/watcher/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/automates/watcher/-/jobs/artifacts/master/raw/integration.svg?job=deploy%20container%20on%20int)](https://gitlab.com/bo-art-of-bonsai/automates/watcher/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/automates/watcher/-/jobs/artifacts/master/raw/production.svg?job=deploy%20container%20on%20prod)](https://gitlab.com/bo-art-of-bonsai/automates/watcher/pipelines) |
| [![](https://gitlab.com/bo-art-of-bonsai/automates/watcher/-/jobs/artifacts/development/raw/python.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/watcher) | [![](https://gitlab.com/bo-art-of-bonsai/automates/watcher/-/jobs/artifacts/master/raw/python.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/watcher) |
| [![](https://gitlab.com/bo-art-of-bonsai/automates/watcher/-/jobs/artifacts/development/raw/bolibrary.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/watcher) | [![](https://gitlab.com/bo-art-of-bonsai/automates/watcher/-/jobs/artifacts/master/raw/bolibrary.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/watcher) |
| [![](https://gitlab.com/bo-art-of-bonsai/automates/watcher/-/jobs/artifacts/development/raw/commits.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/watcher) | [![](https://gitlab.com/bo-art-of-bonsai/automates/watcher/-/jobs/artifacts/master/raw/commits.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/watcher) |
| [![](https://gitlab.com/bo-art-of-bonsai/automates/watcher/-/jobs/artifacts/development/raw/last_commit.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/watcher) | [![](https://gitlab.com/bo-art-of-bonsai/automates/watcher/-/jobs/artifacts/master/raw/last_commit.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/watcher) |
| [![](https://gitlab.com/bo-art-of-bonsai/automates/watcher/-/jobs/artifacts/development/raw/pylint.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/watcher/-/jobs) | |
| [![](https://gitlab.com/bo-art-of-bonsai/automates/watcher/badges/development/coverage.svg)](https://gitlab.com/bo-art-of-bonsai/automates/watcher/-/jobs) | |
 

#### <a href='https://gitlab.com/bo-art-of-bonsai/automates/backup-db'><img src='images/backup-db.png' alt='backup-db' width='32px'/>backup-db</a>

- deploy_type: container
- editor: BO
- coverage: False
- description: Micro-service to backup and restore MySQL database
- technology: MySQL, docker, S3
- repository: https://gitlab.com/bo-art-of-bonsai/automates/backup-db
- environment: development, integration, production
- security: MySQL
- url: 
- doc: 
 

| Delivery                    | Deployment                  |
| :-------------------------- | :-------------------------- |
| [![](https://gitlab.com/bo-art-of-bonsai/automates/backup-db/badges/development/pipeline.svg)](https://gitlab.com/bo-art-of-bonsai/automates/backup-db/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/automates/backup-db/badges/master/pipeline.svg)](https://gitlab.com/bo-art-of-bonsai/automates/backup-db/pipelines) |
| [![](https://gitlab.com/bo-art-of-bonsai/automates/backup-db/-/jobs/artifacts/development/raw/version.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/backup-db/pipelines) | | [![](https://gitlab.com/bo-art-of-bonsai/automates/backup-db/-/jobs/artifacts/master/raw/development.svg?job=deploy%20container%20on%20dev)](https://gitlab.com/bo-art-of-bonsai/automates/backup-db/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/automates/backup-db/-/jobs/artifacts/master/raw/integration.svg?job=deploy%20container%20on%20int)](https://gitlab.com/bo-art-of-bonsai/automates/backup-db/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/automates/backup-db/-/jobs/artifacts/master/raw/production.svg?job=deploy%20container%20on%20prod)](https://gitlab.com/bo-art-of-bonsai/automates/backup-db/pipelines) |
| [![](https://gitlab.com/bo-art-of-bonsai/automates/backup-db/-/jobs/artifacts/development/raw/python.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/backup-db) | [![](https://gitlab.com/bo-art-of-bonsai/automates/backup-db/-/jobs/artifacts/master/raw/python.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/backup-db) |
| [![](https://gitlab.com/bo-art-of-bonsai/automates/backup-db/-/jobs/artifacts/development/raw/bolibrary.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/backup-db) | [![](https://gitlab.com/bo-art-of-bonsai/automates/backup-db/-/jobs/artifacts/master/raw/bolibrary.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/backup-db) |
| [![](https://gitlab.com/bo-art-of-bonsai/automates/backup-db/-/jobs/artifacts/development/raw/commits.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/backup-db) | [![](https://gitlab.com/bo-art-of-bonsai/automates/backup-db/-/jobs/artifacts/master/raw/commits.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/backup-db) |
| [![](https://gitlab.com/bo-art-of-bonsai/automates/backup-db/-/jobs/artifacts/development/raw/last_commit.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/backup-db) | [![](https://gitlab.com/bo-art-of-bonsai/automates/backup-db/-/jobs/artifacts/master/raw/last_commit.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/backup-db) |
| [![](https://gitlab.com/bo-art-of-bonsai/automates/backup-db/-/jobs/artifacts/development/raw/pylint.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/backup-db/-/jobs) | |
| [![](https://gitlab.com/bo-art-of-bonsai/automates/backup-db/badges/development/coverage.svg)](https://gitlab.com/bo-art-of-bonsai/automates/backup-db/-/jobs) | |
 

#### <a href='https://gitlab.com/bo-art-of-bonsai/automates/gitlab-ci-template'><img src='images/gitlab-ci-template.png' alt='gitlab-ci-template' width='32px'/>gitlab-ci-template</a>

- deploy_type: 
- coverage: False
- editor: BO
- description: Templates used by all repositories for CI/CD purpose
- technology: gitlab, yaml
- repository: https://gitlab.com/bo-art-of-bonsai/automates/gitlab-ci-template
- environment: development
- security: Gitlab
- url: 
- doc: 
 

| Delivery                    | Deployment                  |
| :-------------------------- | :-------------------------- |
| [![](https://gitlab.com/bo-art-of-bonsai/automates/gitlab-ci-template/badges/development/pipeline.svg)](https://gitlab.com/bo-art-of-bonsai/automates/gitlab-ci-template/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/automates/gitlab-ci-template/badges/master/pipeline.svg)](https://gitlab.com/bo-art-of-bonsai/automates/gitlab-ci-template/pipelines) |
| [![](https://gitlab.com/bo-art-of-bonsai/automates/gitlab-ci-template/-/jobs/artifacts/development/raw/version.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/gitlab-ci-template/pipelines) | | [![](https://gitlab.com/bo-art-of-bonsai/automates/gitlab-ci-template/-/jobs/artifacts/master/raw/development.svg?job=deploy%20%20on%20dev)](https://gitlab.com/bo-art-of-bonsai/automates/gitlab-ci-template/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/automates/gitlab-ci-template/-/jobs/artifacts/master/raw/integration.svg?job=deploy%20%20on%20int)](https://gitlab.com/bo-art-of-bonsai/automates/gitlab-ci-template/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/automates/gitlab-ci-template/-/jobs/artifacts/master/raw/production.svg?job=deploy%20%20on%20prod)](https://gitlab.com/bo-art-of-bonsai/automates/gitlab-ci-template/pipelines) |
| [![](https://gitlab.com/bo-art-of-bonsai/automates/gitlab-ci-template/-/jobs/artifacts/development/raw/python.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/gitlab-ci-template) | [![](https://gitlab.com/bo-art-of-bonsai/automates/gitlab-ci-template/-/jobs/artifacts/master/raw/python.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/gitlab-ci-template) |
| [![](https://gitlab.com/bo-art-of-bonsai/automates/gitlab-ci-template/-/jobs/artifacts/development/raw/bolibrary.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/gitlab-ci-template) | [![](https://gitlab.com/bo-art-of-bonsai/automates/gitlab-ci-template/-/jobs/artifacts/master/raw/bolibrary.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/gitlab-ci-template) |
| [![](https://gitlab.com/bo-art-of-bonsai/automates/gitlab-ci-template/-/jobs/artifacts/development/raw/commits.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/gitlab-ci-template) | [![](https://gitlab.com/bo-art-of-bonsai/automates/gitlab-ci-template/-/jobs/artifacts/master/raw/commits.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/gitlab-ci-template) |
| [![](https://gitlab.com/bo-art-of-bonsai/automates/gitlab-ci-template/-/jobs/artifacts/development/raw/last_commit.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/gitlab-ci-template) | [![](https://gitlab.com/bo-art-of-bonsai/automates/gitlab-ci-template/-/jobs/artifacts/master/raw/last_commit.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/gitlab-ci-template) |
| [![](https://gitlab.com/bo-art-of-bonsai/automates/gitlab-ci-template/-/jobs/artifacts/development/raw/pylint.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/gitlab-ci-template/-/jobs) | |
| [![](https://gitlab.com/bo-art-of-bonsai/automates/gitlab-ci-template/badges/development/coverage.svg)](https://gitlab.com/bo-art-of-bonsai/automates/gitlab-ci-template/-/jobs) | |
 

#### <a href='https://gitlab.com/bo-art-of-bonsai/automates/gitlab-runner'><img src='images/gitlab-runner.png' alt='gitlab-runner' width='32px'/>gitlab-runner</a>

- deploy_type: container
- coverage: False
- editor: gitlab
- description: Micro-services to run the CI/CD pipelines
- technology: gitlab, docker
- repository: https://gitlab.com/bo-art-of-bonsai/automates/gitlab-runner
- environment: development
- security: Gitlab
- url: 
- doc: 
 

| Delivery                    | Deployment                  |
| :-------------------------- | :-------------------------- |
| [![](https://gitlab.com/bo-art-of-bonsai/automates/gitlab-runner/badges/development/pipeline.svg)](https://gitlab.com/bo-art-of-bonsai/automates/gitlab-runner/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/automates/gitlab-runner/badges/master/pipeline.svg)](https://gitlab.com/bo-art-of-bonsai/automates/gitlab-runner/pipelines) |
| [![](https://gitlab.com/bo-art-of-bonsai/automates/gitlab-runner/-/jobs/artifacts/development/raw/version.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/gitlab-runner/pipelines) | | [![](https://gitlab.com/bo-art-of-bonsai/automates/gitlab-runner/-/jobs/artifacts/master/raw/development.svg?job=deploy%20container%20on%20dev)](https://gitlab.com/bo-art-of-bonsai/automates/gitlab-runner/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/automates/gitlab-runner/-/jobs/artifacts/master/raw/integration.svg?job=deploy%20container%20on%20int)](https://gitlab.com/bo-art-of-bonsai/automates/gitlab-runner/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/automates/gitlab-runner/-/jobs/artifacts/master/raw/production.svg?job=deploy%20container%20on%20prod)](https://gitlab.com/bo-art-of-bonsai/automates/gitlab-runner/pipelines) |
| [![](https://gitlab.com/bo-art-of-bonsai/automates/gitlab-runner/-/jobs/artifacts/development/raw/python.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/gitlab-runner) | [![](https://gitlab.com/bo-art-of-bonsai/automates/gitlab-runner/-/jobs/artifacts/master/raw/python.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/gitlab-runner) |
| [![](https://gitlab.com/bo-art-of-bonsai/automates/gitlab-runner/-/jobs/artifacts/development/raw/bolibrary.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/gitlab-runner) | [![](https://gitlab.com/bo-art-of-bonsai/automates/gitlab-runner/-/jobs/artifacts/master/raw/bolibrary.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/gitlab-runner) |
| [![](https://gitlab.com/bo-art-of-bonsai/automates/gitlab-runner/-/jobs/artifacts/development/raw/commits.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/gitlab-runner) | [![](https://gitlab.com/bo-art-of-bonsai/automates/gitlab-runner/-/jobs/artifacts/master/raw/commits.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/gitlab-runner) |
| [![](https://gitlab.com/bo-art-of-bonsai/automates/gitlab-runner/-/jobs/artifacts/development/raw/last_commit.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/gitlab-runner) | [![](https://gitlab.com/bo-art-of-bonsai/automates/gitlab-runner/-/jobs/artifacts/master/raw/last_commit.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/gitlab-runner) |
| [![](https://gitlab.com/bo-art-of-bonsai/automates/gitlab-runner/-/jobs/artifacts/development/raw/pylint.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/gitlab-runner/-/jobs) | |
| [![](https://gitlab.com/bo-art-of-bonsai/automates/gitlab-runner/badges/development/coverage.svg)](https://gitlab.com/bo-art-of-bonsai/automates/gitlab-runner/-/jobs) | |
 

#### <a href='https://gitlab.com/bo-art-of-bonsai/automates/automation'><img src='images/automation.png' alt='automation' width='32px'/>automation</a>

- deploy_type: 
- coverage: False
- editor: BO
- description: Set of tools to maintain the BO's solution
- technology: shell, python
- repository: https://gitlab.com/bo-art-of-bonsai/automates/automation
- environment: development, backend
- security: no
- url: 
- doc: 
 

| Delivery                    | Deployment                  |
| :-------------------------- | :-------------------------- |
| [![](https://gitlab.com/bo-art-of-bonsai/automates/automation/badges/development/pipeline.svg)](https://gitlab.com/bo-art-of-bonsai/automates/automation/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/automates/automation/badges/master/pipeline.svg)](https://gitlab.com/bo-art-of-bonsai/automates/automation/pipelines) |
| [![](https://gitlab.com/bo-art-of-bonsai/automates/automation/-/jobs/artifacts/development/raw/version.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/automation/pipelines) | | [![](https://gitlab.com/bo-art-of-bonsai/automates/automation/-/jobs/artifacts/master/raw/development.svg?job=deploy%20%20on%20dev)](https://gitlab.com/bo-art-of-bonsai/automates/automation/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/automates/automation/-/jobs/artifacts/master/raw/integration.svg?job=deploy%20%20on%20int)](https://gitlab.com/bo-art-of-bonsai/automates/automation/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/automates/automation/-/jobs/artifacts/master/raw/production.svg?job=deploy%20%20on%20prod)](https://gitlab.com/bo-art-of-bonsai/automates/automation/pipelines) |
| [![](https://gitlab.com/bo-art-of-bonsai/automates/automation/-/jobs/artifacts/development/raw/python.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/automation) | [![](https://gitlab.com/bo-art-of-bonsai/automates/automation/-/jobs/artifacts/master/raw/python.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/automation) |
| [![](https://gitlab.com/bo-art-of-bonsai/automates/automation/-/jobs/artifacts/development/raw/bolibrary.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/automation) | [![](https://gitlab.com/bo-art-of-bonsai/automates/automation/-/jobs/artifacts/master/raw/bolibrary.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/automation) |
| [![](https://gitlab.com/bo-art-of-bonsai/automates/automation/-/jobs/artifacts/development/raw/commits.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/automation) | [![](https://gitlab.com/bo-art-of-bonsai/automates/automation/-/jobs/artifacts/master/raw/commits.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/automation) |
| [![](https://gitlab.com/bo-art-of-bonsai/automates/automation/-/jobs/artifacts/development/raw/last_commit.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/automation) | [![](https://gitlab.com/bo-art-of-bonsai/automates/automation/-/jobs/artifacts/master/raw/last_commit.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/automation) |
| [![](https://gitlab.com/bo-art-of-bonsai/automates/automation/-/jobs/artifacts/development/raw/pylint.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/automation/-/jobs) | |
| [![](https://gitlab.com/bo-art-of-bonsai/automates/automation/badges/development/coverage.svg)](https://gitlab.com/bo-art-of-bonsai/automates/automation/-/jobs) | |
 

#### <a href='https://gitlab.com/bo-art-of-bonsai/automates/mastering'><img src='images/mastering.png' alt='mastering' width='32px'/>mastering</a>

- deploy_type: 
- coverage: True
- editor: BO
- description: Deploy and manage the BO's IT asset 
- technology: ansible, python, json
- repository: https://gitlab.com/bo-art-of-bonsai/automates/mastering
- environment: development, integration, production
- security: AWS
- url: 
- doc: 
 

| Delivery                    | Deployment                  |
| :-------------------------- | :-------------------------- |
| [![](https://gitlab.com/bo-art-of-bonsai/automates/mastering/badges/development/pipeline.svg)](https://gitlab.com/bo-art-of-bonsai/automates/mastering/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/automates/mastering/badges/master/pipeline.svg)](https://gitlab.com/bo-art-of-bonsai/automates/mastering/pipelines) |
| [![](https://gitlab.com/bo-art-of-bonsai/automates/mastering/-/jobs/artifacts/development/raw/version.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/mastering/pipelines) | | [![](https://gitlab.com/bo-art-of-bonsai/automates/mastering/-/jobs/artifacts/master/raw/development.svg?job=deploy%20%20on%20dev)](https://gitlab.com/bo-art-of-bonsai/automates/mastering/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/automates/mastering/-/jobs/artifacts/master/raw/integration.svg?job=deploy%20%20on%20int)](https://gitlab.com/bo-art-of-bonsai/automates/mastering/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/automates/mastering/-/jobs/artifacts/master/raw/production.svg?job=deploy%20%20on%20prod)](https://gitlab.com/bo-art-of-bonsai/automates/mastering/pipelines) |
| [![](https://gitlab.com/bo-art-of-bonsai/automates/mastering/-/jobs/artifacts/development/raw/python.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/mastering) | [![](https://gitlab.com/bo-art-of-bonsai/automates/mastering/-/jobs/artifacts/master/raw/python.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/mastering) |
| [![](https://gitlab.com/bo-art-of-bonsai/automates/mastering/-/jobs/artifacts/development/raw/bolibrary.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/mastering) | [![](https://gitlab.com/bo-art-of-bonsai/automates/mastering/-/jobs/artifacts/master/raw/bolibrary.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/mastering) |
| [![](https://gitlab.com/bo-art-of-bonsai/automates/mastering/-/jobs/artifacts/development/raw/commits.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/mastering) | [![](https://gitlab.com/bo-art-of-bonsai/automates/mastering/-/jobs/artifacts/master/raw/commits.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/mastering) |
| [![](https://gitlab.com/bo-art-of-bonsai/automates/mastering/-/jobs/artifacts/development/raw/last_commit.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/mastering) | [![](https://gitlab.com/bo-art-of-bonsai/automates/mastering/-/jobs/artifacts/master/raw/last_commit.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/mastering) |
| [![](https://gitlab.com/bo-art-of-bonsai/automates/mastering/-/jobs/artifacts/development/raw/pylint.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/mastering/-/jobs) | |
| [![](https://gitlab.com/bo-art-of-bonsai/automates/mastering/badges/development/coverage.svg)](https://gitlab.com/bo-art-of-bonsai/automates/mastering/-/jobs) | |
 

#### <a href='https://gitlab.com/bo-art-of-bonsai/automates/test'><img src='images/test.png' alt='test' width='32px'/>test</a>

- deploy_type: serverless
- coverage: True
- editor: BO
- description: Useful for test
- technology: python, lambda, docker
- repository: https://gitlab.com/bo-art-of-bonsai/automates/test
- environment: development, integration, production
- security: no
- url: 
- doc: 
 

| Delivery                    | Deployment                  |
| :-------------------------- | :-------------------------- |
| [![](https://gitlab.com/bo-art-of-bonsai/automates/test/badges/development/pipeline.svg)](https://gitlab.com/bo-art-of-bonsai/automates/test/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/automates/test/badges/master/pipeline.svg)](https://gitlab.com/bo-art-of-bonsai/automates/test/pipelines) |
| [![](https://gitlab.com/bo-art-of-bonsai/automates/test/-/jobs/artifacts/development/raw/version.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/test/pipelines) | | [![](https://gitlab.com/bo-art-of-bonsai/automates/test/-/jobs/artifacts/master/raw/development.svg?job=deploy%20serverless%20on%20dev)](https://gitlab.com/bo-art-of-bonsai/automates/test/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/automates/test/-/jobs/artifacts/master/raw/integration.svg?job=deploy%20serverless%20on%20int)](https://gitlab.com/bo-art-of-bonsai/automates/test/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/automates/test/-/jobs/artifacts/master/raw/production.svg?job=deploy%20serverless%20on%20prod)](https://gitlab.com/bo-art-of-bonsai/automates/test/pipelines) |
| [![](https://gitlab.com/bo-art-of-bonsai/automates/test/-/jobs/artifacts/development/raw/python.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/test) | [![](https://gitlab.com/bo-art-of-bonsai/automates/test/-/jobs/artifacts/master/raw/python.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/test) |
| [![](https://gitlab.com/bo-art-of-bonsai/automates/test/-/jobs/artifacts/development/raw/bolibrary.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/test) | [![](https://gitlab.com/bo-art-of-bonsai/automates/test/-/jobs/artifacts/master/raw/bolibrary.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/test) |
| [![](https://gitlab.com/bo-art-of-bonsai/automates/test/-/jobs/artifacts/development/raw/commits.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/test) | [![](https://gitlab.com/bo-art-of-bonsai/automates/test/-/jobs/artifacts/master/raw/commits.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/test) |
| [![](https://gitlab.com/bo-art-of-bonsai/automates/test/-/jobs/artifacts/development/raw/last_commit.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/test) | [![](https://gitlab.com/bo-art-of-bonsai/automates/test/-/jobs/artifacts/master/raw/last_commit.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/test) |
| [![](https://gitlab.com/bo-art-of-bonsai/automates/test/-/jobs/artifacts/development/raw/pylint.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/automates/test/-/jobs) | |
| [![](https://gitlab.com/bo-art-of-bonsai/automates/test/badges/development/coverage.svg)](https://gitlab.com/bo-art-of-bonsai/automates/test/-/jobs) | |
 

### Archived
#### <a href=''><img src='images/mobile.png' alt='mobile' width='32px'/>mobile</a>

- deploy_type: host
- editor: bleu122
- coverage: False
- description: Application to server the Smart-application
- technology: bleu122, mysql
- repository: 
- environment: integration
- security: MFA, basic auth
- url: 
- doc: 
 

| Delivery                    | Deployment                  |
| :-------------------------- | :-------------------------- |
| [![](/badges/development/pipeline.svg)](/pipelines) | [![](/badges/master/pipeline.svg)](/pipelines) |
| [![](/-/jobs/artifacts/development/raw/version.svg?job=badges%20setup)](/pipelines) | | [![](/-/jobs/artifacts/master/raw/development.svg?job=deploy%20host%20on%20dev)](/pipelines) | [![](/-/jobs/artifacts/master/raw/integration.svg?job=deploy%20host%20on%20int)](/pipelines) | [![](/-/jobs/artifacts/master/raw/production.svg?job=deploy%20host%20on%20prod)](/pipelines) |
| [![](/-/jobs/artifacts/development/raw/python.svg?job=badges%20setup)]() | [![](/-/jobs/artifacts/master/raw/python.svg?job=badges%20setup)]() |
| [![](/-/jobs/artifacts/development/raw/bolibrary.svg?job=badges%20setup)]() | [![](/-/jobs/artifacts/master/raw/bolibrary.svg?job=badges%20setup)]() |
| [![](/-/jobs/artifacts/development/raw/commits.svg?job=badges%20setup)]() | [![](/-/jobs/artifacts/master/raw/commits.svg?job=badges%20setup)]() |
| [![](/-/jobs/artifacts/development/raw/last_commit.svg?job=badges%20setup)]() | [![](/-/jobs/artifacts/master/raw/last_commit.svg?job=badges%20setup)]() |
| [![](/-/jobs/artifacts/development/raw/pylint.svg?job=badges%20setup)](/-/jobs) | |
| [![](/badges/development/coverage.svg)](/-/jobs) | |
 

#### <a href=''><img src='images/iphone.png' alt='iphone' width='32px'/>iphone</a>

- deploy_type: mobile
- editor: bleu122
- coverage: False
- description: Smart-application for IPhone and Androïd
- technology: bleu122
- repository: 
- environment: integration
- security: MFA, basic auth
- url: 
- doc: 
 

| Delivery                    | Deployment                  |
| :-------------------------- | :-------------------------- |
| [![](/badges/development/pipeline.svg)](/pipelines) | [![](/badges/master/pipeline.svg)](/pipelines) |
| [![](/-/jobs/artifacts/development/raw/version.svg?job=badges%20setup)](/pipelines) | | [![](/-/jobs/artifacts/master/raw/development.svg?job=deploy%20mobile%20on%20dev)](/pipelines) | [![](/-/jobs/artifacts/master/raw/integration.svg?job=deploy%20mobile%20on%20int)](/pipelines) | [![](/-/jobs/artifacts/master/raw/production.svg?job=deploy%20mobile%20on%20prod)](/pipelines) |
| [![](/-/jobs/artifacts/development/raw/python.svg?job=badges%20setup)]() | [![](/-/jobs/artifacts/master/raw/python.svg?job=badges%20setup)]() |
| [![](/-/jobs/artifacts/development/raw/bolibrary.svg?job=badges%20setup)]() | [![](/-/jobs/artifacts/master/raw/bolibrary.svg?job=badges%20setup)]() |
| [![](/-/jobs/artifacts/development/raw/commits.svg?job=badges%20setup)]() | [![](/-/jobs/artifacts/master/raw/commits.svg?job=badges%20setup)]() |
| [![](/-/jobs/artifacts/development/raw/last_commit.svg?job=badges%20setup)]() | [![](/-/jobs/artifacts/master/raw/last_commit.svg?job=badges%20setup)]() |
| [![](/-/jobs/artifacts/development/raw/pylint.svg?job=badges%20setup)](/-/jobs) | |
| [![](/badges/development/coverage.svg)](/-/jobs) | |
 

#### <a href='https://gitlab.com/bo-art-of-bonsai/archived/smart-app'><img src='images/smart-app.png' alt='smart-app' width='32px'/>smart-app</a>

- deploy_type: mobile
- editor: neomind
- framework: neomind
- coverage: False
- description: Useful for test
- technology: androïd
- repository: https://gitlab.com/bo-art-of-bonsai/archived/smart-app
- environment: production
- security: basic auth
- url: 
- doc: 
 

| Delivery                    | Deployment                  |
| :-------------------------- | :-------------------------- |
| [![](https://gitlab.com/bo-art-of-bonsai/archived/smart-app/badges/development/pipeline.svg)](https://gitlab.com/bo-art-of-bonsai/archived/smart-app/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/archived/smart-app/badges/master/pipeline.svg)](https://gitlab.com/bo-art-of-bonsai/archived/smart-app/pipelines) |
| [![](https://gitlab.com/bo-art-of-bonsai/archived/smart-app/-/jobs/artifacts/development/raw/version.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/smart-app/pipelines) | | [![](https://gitlab.com/bo-art-of-bonsai/archived/smart-app/-/jobs/artifacts/master/raw/development.svg?job=deploy%20mobile%20on%20dev)](https://gitlab.com/bo-art-of-bonsai/archived/smart-app/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/archived/smart-app/-/jobs/artifacts/master/raw/integration.svg?job=deploy%20mobile%20on%20int)](https://gitlab.com/bo-art-of-bonsai/archived/smart-app/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/archived/smart-app/-/jobs/artifacts/master/raw/production.svg?job=deploy%20mobile%20on%20prod)](https://gitlab.com/bo-art-of-bonsai/archived/smart-app/pipelines) |
| [![](https://gitlab.com/bo-art-of-bonsai/archived/smart-app/-/jobs/artifacts/development/raw/python.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/smart-app) | [![](https://gitlab.com/bo-art-of-bonsai/archived/smart-app/-/jobs/artifacts/master/raw/python.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/smart-app) |
| [![](https://gitlab.com/bo-art-of-bonsai/archived/smart-app/-/jobs/artifacts/development/raw/bolibrary.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/smart-app) | [![](https://gitlab.com/bo-art-of-bonsai/archived/smart-app/-/jobs/artifacts/master/raw/bolibrary.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/smart-app) |
| [![](https://gitlab.com/bo-art-of-bonsai/archived/smart-app/-/jobs/artifacts/development/raw/commits.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/smart-app) | [![](https://gitlab.com/bo-art-of-bonsai/archived/smart-app/-/jobs/artifacts/master/raw/commits.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/smart-app) |
| [![](https://gitlab.com/bo-art-of-bonsai/archived/smart-app/-/jobs/artifacts/development/raw/last_commit.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/smart-app) | [![](https://gitlab.com/bo-art-of-bonsai/archived/smart-app/-/jobs/artifacts/master/raw/last_commit.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/smart-app) |
| [![](https://gitlab.com/bo-art-of-bonsai/archived/smart-app/-/jobs/artifacts/development/raw/pylint.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/smart-app/-/jobs) | |
| [![](https://gitlab.com/bo-art-of-bonsai/archived/smart-app/badges/development/coverage.svg)](https://gitlab.com/bo-art-of-bonsai/archived/smart-app/-/jobs) | |
 

#### <a href='https://gitlab.com/bo-art-of-bonsai/archived/app'><img src='images/app.png' alt='app' width='32px'/>app</a>

- deploy_type: container
- editor: neomind
- framework: laravel
- coverage: False
- description: Useful for test
- technology: php, laravel, mysql, docker
- repository: https://gitlab.com/bo-art-of-bonsai/archived/app
- environment: development,integration,production
- security: ssl, basic auth
- url: 
- doc: 
 

| Delivery                    | Deployment                  |
| :-------------------------- | :-------------------------- |
| [![](https://gitlab.com/bo-art-of-bonsai/archived/app/badges/development/pipeline.svg)](https://gitlab.com/bo-art-of-bonsai/archived/app/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/archived/app/badges/master/pipeline.svg)](https://gitlab.com/bo-art-of-bonsai/archived/app/pipelines) |
| [![](https://gitlab.com/bo-art-of-bonsai/archived/app/-/jobs/artifacts/development/raw/version.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/app/pipelines) | | [![](https://gitlab.com/bo-art-of-bonsai/archived/app/-/jobs/artifacts/master/raw/development.svg?job=deploy%20container%20on%20dev)](https://gitlab.com/bo-art-of-bonsai/archived/app/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/archived/app/-/jobs/artifacts/master/raw/integration.svg?job=deploy%20container%20on%20int)](https://gitlab.com/bo-art-of-bonsai/archived/app/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/archived/app/-/jobs/artifacts/master/raw/production.svg?job=deploy%20container%20on%20prod)](https://gitlab.com/bo-art-of-bonsai/archived/app/pipelines) |
| [![](https://gitlab.com/bo-art-of-bonsai/archived/app/-/jobs/artifacts/development/raw/python.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/app) | [![](https://gitlab.com/bo-art-of-bonsai/archived/app/-/jobs/artifacts/master/raw/python.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/app) |
| [![](https://gitlab.com/bo-art-of-bonsai/archived/app/-/jobs/artifacts/development/raw/bolibrary.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/app) | [![](https://gitlab.com/bo-art-of-bonsai/archived/app/-/jobs/artifacts/master/raw/bolibrary.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/app) |
| [![](https://gitlab.com/bo-art-of-bonsai/archived/app/-/jobs/artifacts/development/raw/commits.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/app) | [![](https://gitlab.com/bo-art-of-bonsai/archived/app/-/jobs/artifacts/master/raw/commits.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/app) |
| [![](https://gitlab.com/bo-art-of-bonsai/archived/app/-/jobs/artifacts/development/raw/last_commit.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/app) | [![](https://gitlab.com/bo-art-of-bonsai/archived/app/-/jobs/artifacts/master/raw/last_commit.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/app) |
| [![](https://gitlab.com/bo-art-of-bonsai/archived/app/-/jobs/artifacts/development/raw/pylint.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/app/-/jobs) | |
| [![](https://gitlab.com/bo-art-of-bonsai/archived/app/badges/development/coverage.svg)](https://gitlab.com/bo-art-of-bonsai/archived/app/-/jobs) | |
 

#### <a href='https://gitlab.com/bo-art-of-bonsai/archived/api-app'><img src='images/api-app.png' alt='api-app' width='32px'/>api-app</a>

- deploy_type: container
- editor: neomind
- framework: laravel
- coverage: False
- description: Useful for test
- technology: php, laravel, docker
- repository: https://gitlab.com/bo-art-of-bonsai/archived/api-app
- environment: development,integration,production
- security: ssl, basic auth
- url: 
- doc: 
 

| Delivery                    | Deployment                  |
| :-------------------------- | :-------------------------- |
| [![](https://gitlab.com/bo-art-of-bonsai/archived/api-app/badges/development/pipeline.svg)](https://gitlab.com/bo-art-of-bonsai/archived/api-app/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/archived/api-app/badges/master/pipeline.svg)](https://gitlab.com/bo-art-of-bonsai/archived/api-app/pipelines) |
| [![](https://gitlab.com/bo-art-of-bonsai/archived/api-app/-/jobs/artifacts/development/raw/version.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/api-app/pipelines) | | [![](https://gitlab.com/bo-art-of-bonsai/archived/api-app/-/jobs/artifacts/master/raw/development.svg?job=deploy%20container%20on%20dev)](https://gitlab.com/bo-art-of-bonsai/archived/api-app/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/archived/api-app/-/jobs/artifacts/master/raw/integration.svg?job=deploy%20container%20on%20int)](https://gitlab.com/bo-art-of-bonsai/archived/api-app/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/archived/api-app/-/jobs/artifacts/master/raw/production.svg?job=deploy%20container%20on%20prod)](https://gitlab.com/bo-art-of-bonsai/archived/api-app/pipelines) |
| [![](https://gitlab.com/bo-art-of-bonsai/archived/api-app/-/jobs/artifacts/development/raw/python.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/api-app) | [![](https://gitlab.com/bo-art-of-bonsai/archived/api-app/-/jobs/artifacts/master/raw/python.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/api-app) |
| [![](https://gitlab.com/bo-art-of-bonsai/archived/api-app/-/jobs/artifacts/development/raw/bolibrary.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/api-app) | [![](https://gitlab.com/bo-art-of-bonsai/archived/api-app/-/jobs/artifacts/master/raw/bolibrary.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/api-app) |
| [![](https://gitlab.com/bo-art-of-bonsai/archived/api-app/-/jobs/artifacts/development/raw/commits.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/api-app) | [![](https://gitlab.com/bo-art-of-bonsai/archived/api-app/-/jobs/artifacts/master/raw/commits.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/api-app) |
| [![](https://gitlab.com/bo-art-of-bonsai/archived/api-app/-/jobs/artifacts/development/raw/last_commit.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/api-app) | [![](https://gitlab.com/bo-art-of-bonsai/archived/api-app/-/jobs/artifacts/master/raw/last_commit.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/api-app) |
| [![](https://gitlab.com/bo-art-of-bonsai/archived/api-app/-/jobs/artifacts/development/raw/pylint.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/api-app/-/jobs) | |
| [![](https://gitlab.com/bo-art-of-bonsai/archived/api-app/badges/development/coverage.svg)](https://gitlab.com/bo-art-of-bonsai/archived/api-app/-/jobs) | |
 

#### <a href='https://gitlab.com/bo-art-of-bonsai/archived/api-gw'><img src='images/api-gw.png' alt='api-gw' width='32px'/>api-gw</a>

- deploy_type: container
- editor: BO
- coverage: True
- description: Useful for test
- technology: python, openapi, docker
- repository: https://gitlab.com/bo-art-of-bonsai/archived/api-gw
- environment: development,integration,production
- security: ssl, basic auth
- url: 
- doc: 
 

| Delivery                    | Deployment                  |
| :-------------------------- | :-------------------------- |
| [![](https://gitlab.com/bo-art-of-bonsai/archived/api-gw/badges/development/pipeline.svg)](https://gitlab.com/bo-art-of-bonsai/archived/api-gw/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/archived/api-gw/badges/master/pipeline.svg)](https://gitlab.com/bo-art-of-bonsai/archived/api-gw/pipelines) |
| [![](https://gitlab.com/bo-art-of-bonsai/archived/api-gw/-/jobs/artifacts/development/raw/version.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/api-gw/pipelines) | | [![](https://gitlab.com/bo-art-of-bonsai/archived/api-gw/-/jobs/artifacts/master/raw/development.svg?job=deploy%20container%20on%20dev)](https://gitlab.com/bo-art-of-bonsai/archived/api-gw/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/archived/api-gw/-/jobs/artifacts/master/raw/integration.svg?job=deploy%20container%20on%20int)](https://gitlab.com/bo-art-of-bonsai/archived/api-gw/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/archived/api-gw/-/jobs/artifacts/master/raw/production.svg?job=deploy%20container%20on%20prod)](https://gitlab.com/bo-art-of-bonsai/archived/api-gw/pipelines) |
| [![](https://gitlab.com/bo-art-of-bonsai/archived/api-gw/-/jobs/artifacts/development/raw/python.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/api-gw) | [![](https://gitlab.com/bo-art-of-bonsai/archived/api-gw/-/jobs/artifacts/master/raw/python.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/api-gw) |
| [![](https://gitlab.com/bo-art-of-bonsai/archived/api-gw/-/jobs/artifacts/development/raw/bolibrary.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/api-gw) | [![](https://gitlab.com/bo-art-of-bonsai/archived/api-gw/-/jobs/artifacts/master/raw/bolibrary.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/api-gw) |
| [![](https://gitlab.com/bo-art-of-bonsai/archived/api-gw/-/jobs/artifacts/development/raw/commits.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/api-gw) | [![](https://gitlab.com/bo-art-of-bonsai/archived/api-gw/-/jobs/artifacts/master/raw/commits.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/api-gw) |
| [![](https://gitlab.com/bo-art-of-bonsai/archived/api-gw/-/jobs/artifacts/development/raw/last_commit.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/api-gw) | [![](https://gitlab.com/bo-art-of-bonsai/archived/api-gw/-/jobs/artifacts/master/raw/last_commit.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/api-gw) |
| [![](https://gitlab.com/bo-art-of-bonsai/archived/api-gw/-/jobs/artifacts/development/raw/pylint.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/api-gw/-/jobs) | |
| [![](https://gitlab.com/bo-art-of-bonsai/archived/api-gw/badges/development/coverage.svg)](https://gitlab.com/bo-art-of-bonsai/archived/api-gw/-/jobs) | |
 

#### <a href='https://gitlab.com/bo-art-of-bonsai/archived/automates/plantuml'><img src='images/plantuml.png' alt='plantuml' width='32px'/>plantuml</a>

- deploy_type: container
- editor: BO
- coverage: False
- description: Useful for test
- technology: python, plantuml, docker
- repository: https://gitlab.com/bo-art-of-bonsai/archived/automates/plantuml
- environment: development,backend
- security: no
- url: 
- doc: 
 

| Delivery                    | Deployment                  |
| :-------------------------- | :-------------------------- |
| [![](https://gitlab.com/bo-art-of-bonsai/archived/automates/plantuml/badges/development/pipeline.svg)](https://gitlab.com/bo-art-of-bonsai/archived/automates/plantuml/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/archived/automates/plantuml/badges/master/pipeline.svg)](https://gitlab.com/bo-art-of-bonsai/archived/automates/plantuml/pipelines) |
| [![](https://gitlab.com/bo-art-of-bonsai/archived/automates/plantuml/-/jobs/artifacts/development/raw/version.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/automates/plantuml/pipelines) | | [![](https://gitlab.com/bo-art-of-bonsai/archived/automates/plantuml/-/jobs/artifacts/master/raw/development.svg?job=deploy%20container%20on%20dev)](https://gitlab.com/bo-art-of-bonsai/archived/automates/plantuml/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/archived/automates/plantuml/-/jobs/artifacts/master/raw/integration.svg?job=deploy%20container%20on%20int)](https://gitlab.com/bo-art-of-bonsai/archived/automates/plantuml/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/archived/automates/plantuml/-/jobs/artifacts/master/raw/production.svg?job=deploy%20container%20on%20prod)](https://gitlab.com/bo-art-of-bonsai/archived/automates/plantuml/pipelines) |
| [![](https://gitlab.com/bo-art-of-bonsai/archived/automates/plantuml/-/jobs/artifacts/development/raw/python.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/automates/plantuml) | [![](https://gitlab.com/bo-art-of-bonsai/archived/automates/plantuml/-/jobs/artifacts/master/raw/python.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/automates/plantuml) |
| [![](https://gitlab.com/bo-art-of-bonsai/archived/automates/plantuml/-/jobs/artifacts/development/raw/bolibrary.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/automates/plantuml) | [![](https://gitlab.com/bo-art-of-bonsai/archived/automates/plantuml/-/jobs/artifacts/master/raw/bolibrary.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/automates/plantuml) |
| [![](https://gitlab.com/bo-art-of-bonsai/archived/automates/plantuml/-/jobs/artifacts/development/raw/commits.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/automates/plantuml) | [![](https://gitlab.com/bo-art-of-bonsai/archived/automates/plantuml/-/jobs/artifacts/master/raw/commits.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/automates/plantuml) |
| [![](https://gitlab.com/bo-art-of-bonsai/archived/automates/plantuml/-/jobs/artifacts/development/raw/last_commit.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/automates/plantuml) | [![](https://gitlab.com/bo-art-of-bonsai/archived/automates/plantuml/-/jobs/artifacts/master/raw/last_commit.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/automates/plantuml) |
| [![](https://gitlab.com/bo-art-of-bonsai/archived/automates/plantuml/-/jobs/artifacts/development/raw/pylint.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/automates/plantuml/-/jobs) | |
| [![](https://gitlab.com/bo-art-of-bonsai/archived/automates/plantuml/badges/development/coverage.svg)](https://gitlab.com/bo-art-of-bonsai/archived/automates/plantuml/-/jobs) | |
 

#### <a href='https://gitlab.com/bo-art-of-bonsai/archived/automates/postman'><img src='images/postman.png' alt='postman' width='32px'/>postman</a>

- deploy_type: serverless
- editor: BO
- coverage: True
- description: Useful for test
- technology: serverless, postman, api, python
- repository: https://gitlab.com/bo-art-of-bonsai/archived/automates/postman
- environment: development,backend
- security: no
- url: 
- doc: 
 

| Delivery                    | Deployment                  |
| :-------------------------- | :-------------------------- |
| [![](https://gitlab.com/bo-art-of-bonsai/archived/automates/postman/badges/development/pipeline.svg)](https://gitlab.com/bo-art-of-bonsai/archived/automates/postman/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/archived/automates/postman/badges/master/pipeline.svg)](https://gitlab.com/bo-art-of-bonsai/archived/automates/postman/pipelines) |
| [![](https://gitlab.com/bo-art-of-bonsai/archived/automates/postman/-/jobs/artifacts/development/raw/version.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/automates/postman/pipelines) | | [![](https://gitlab.com/bo-art-of-bonsai/archived/automates/postman/-/jobs/artifacts/master/raw/development.svg?job=deploy%20serverless%20on%20dev)](https://gitlab.com/bo-art-of-bonsai/archived/automates/postman/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/archived/automates/postman/-/jobs/artifacts/master/raw/integration.svg?job=deploy%20serverless%20on%20int)](https://gitlab.com/bo-art-of-bonsai/archived/automates/postman/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/archived/automates/postman/-/jobs/artifacts/master/raw/production.svg?job=deploy%20serverless%20on%20prod)](https://gitlab.com/bo-art-of-bonsai/archived/automates/postman/pipelines) |
| [![](https://gitlab.com/bo-art-of-bonsai/archived/automates/postman/-/jobs/artifacts/development/raw/python.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/automates/postman) | [![](https://gitlab.com/bo-art-of-bonsai/archived/automates/postman/-/jobs/artifacts/master/raw/python.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/automates/postman) |
| [![](https://gitlab.com/bo-art-of-bonsai/archived/automates/postman/-/jobs/artifacts/development/raw/bolibrary.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/automates/postman) | [![](https://gitlab.com/bo-art-of-bonsai/archived/automates/postman/-/jobs/artifacts/master/raw/bolibrary.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/automates/postman) |
| [![](https://gitlab.com/bo-art-of-bonsai/archived/automates/postman/-/jobs/artifacts/development/raw/commits.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/automates/postman) | [![](https://gitlab.com/bo-art-of-bonsai/archived/automates/postman/-/jobs/artifacts/master/raw/commits.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/automates/postman) |
| [![](https://gitlab.com/bo-art-of-bonsai/archived/automates/postman/-/jobs/artifacts/development/raw/last_commit.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/automates/postman) | [![](https://gitlab.com/bo-art-of-bonsai/archived/automates/postman/-/jobs/artifacts/master/raw/last_commit.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/automates/postman) |
| [![](https://gitlab.com/bo-art-of-bonsai/archived/automates/postman/-/jobs/artifacts/development/raw/pylint.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/automates/postman/-/jobs) | |
| [![](https://gitlab.com/bo-art-of-bonsai/archived/automates/postman/badges/development/coverage.svg)](https://gitlab.com/bo-art-of-bonsai/archived/automates/postman/-/jobs) | |
 

#### <a href='https://gitlab.com/bo-art-of-bonsai/archived/automates/swagger-to-diagram'><img src='images/swagger-to-diagram.png' alt='swagger-to-diagram' width='32px'/>swagger-to-diagram</a>

- deploy_type: Container
- editor: BO
- coverage: False
- description: Schema generator from swagger definition
- technology: python, swagger, docker
- repository: https://gitlab.com/bo-art-of-bonsai/archived/automates/swagger-to-diagram
- environment: development,backend
- security: no
- url: 
- doc: 
 

| Delivery                    | Deployment                  |
| :-------------------------- | :-------------------------- |
| [![](https://gitlab.com/bo-art-of-bonsai/archived/automates/swagger-to-diagram/badges/development/pipeline.svg)](https://gitlab.com/bo-art-of-bonsai/archived/automates/swagger-to-diagram/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/archived/automates/swagger-to-diagram/badges/master/pipeline.svg)](https://gitlab.com/bo-art-of-bonsai/archived/automates/swagger-to-diagram/pipelines) |
| [![](https://gitlab.com/bo-art-of-bonsai/archived/automates/swagger-to-diagram/-/jobs/artifacts/development/raw/version.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/automates/swagger-to-diagram/pipelines) | | [![](https://gitlab.com/bo-art-of-bonsai/archived/automates/swagger-to-diagram/-/jobs/artifacts/master/raw/development.svg?job=deploy%20Container%20on%20dev)](https://gitlab.com/bo-art-of-bonsai/archived/automates/swagger-to-diagram/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/archived/automates/swagger-to-diagram/-/jobs/artifacts/master/raw/integration.svg?job=deploy%20Container%20on%20int)](https://gitlab.com/bo-art-of-bonsai/archived/automates/swagger-to-diagram/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/archived/automates/swagger-to-diagram/-/jobs/artifacts/master/raw/production.svg?job=deploy%20Container%20on%20prod)](https://gitlab.com/bo-art-of-bonsai/archived/automates/swagger-to-diagram/pipelines) |
| [![](https://gitlab.com/bo-art-of-bonsai/archived/automates/swagger-to-diagram/-/jobs/artifacts/development/raw/python.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/automates/swagger-to-diagram) | [![](https://gitlab.com/bo-art-of-bonsai/archived/automates/swagger-to-diagram/-/jobs/artifacts/master/raw/python.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/automates/swagger-to-diagram) |
| [![](https://gitlab.com/bo-art-of-bonsai/archived/automates/swagger-to-diagram/-/jobs/artifacts/development/raw/bolibrary.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/automates/swagger-to-diagram) | [![](https://gitlab.com/bo-art-of-bonsai/archived/automates/swagger-to-diagram/-/jobs/artifacts/master/raw/bolibrary.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/automates/swagger-to-diagram) |
| [![](https://gitlab.com/bo-art-of-bonsai/archived/automates/swagger-to-diagram/-/jobs/artifacts/development/raw/commits.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/automates/swagger-to-diagram) | [![](https://gitlab.com/bo-art-of-bonsai/archived/automates/swagger-to-diagram/-/jobs/artifacts/master/raw/commits.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/automates/swagger-to-diagram) |
| [![](https://gitlab.com/bo-art-of-bonsai/archived/automates/swagger-to-diagram/-/jobs/artifacts/development/raw/last_commit.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/automates/swagger-to-diagram) | [![](https://gitlab.com/bo-art-of-bonsai/archived/automates/swagger-to-diagram/-/jobs/artifacts/master/raw/last_commit.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/automates/swagger-to-diagram) |
| [![](https://gitlab.com/bo-art-of-bonsai/archived/automates/swagger-to-diagram/-/jobs/artifacts/development/raw/pylint.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/automates/swagger-to-diagram/-/jobs) | |
| [![](https://gitlab.com/bo-art-of-bonsai/archived/automates/swagger-to-diagram/badges/development/coverage.svg)](https://gitlab.com/bo-art-of-bonsai/archived/automates/swagger-to-diagram/-/jobs) | |
 

#### <a href='https://gitlab.com/bo-art-of-bonsai/archived/automates/openapi-codegen'><img src='images/openapi-codegen.png' alt='openapi-codegen' width='32px'/>openapi-codegen</a>

- deploy_type: Container
- editor: swagger
- coverage: False
- description: API generator from json/yaml definition
- technology: python, openapi
- repository: https://gitlab.com/bo-art-of-bonsai/archived/automates/openapi-codegen
- environment: development,backend
- security: no
- url: 
- doc: 
 

| Delivery                    | Deployment                  |
| :-------------------------- | :-------------------------- |
| [![](https://gitlab.com/bo-art-of-bonsai/archived/automates/openapi-codegen/badges/development/pipeline.svg)](https://gitlab.com/bo-art-of-bonsai/archived/automates/openapi-codegen/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/archived/automates/openapi-codegen/badges/master/pipeline.svg)](https://gitlab.com/bo-art-of-bonsai/archived/automates/openapi-codegen/pipelines) |
| [![](https://gitlab.com/bo-art-of-bonsai/archived/automates/openapi-codegen/-/jobs/artifacts/development/raw/version.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/automates/openapi-codegen/pipelines) | | [![](https://gitlab.com/bo-art-of-bonsai/archived/automates/openapi-codegen/-/jobs/artifacts/master/raw/development.svg?job=deploy%20Container%20on%20dev)](https://gitlab.com/bo-art-of-bonsai/archived/automates/openapi-codegen/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/archived/automates/openapi-codegen/-/jobs/artifacts/master/raw/integration.svg?job=deploy%20Container%20on%20int)](https://gitlab.com/bo-art-of-bonsai/archived/automates/openapi-codegen/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/archived/automates/openapi-codegen/-/jobs/artifacts/master/raw/production.svg?job=deploy%20Container%20on%20prod)](https://gitlab.com/bo-art-of-bonsai/archived/automates/openapi-codegen/pipelines) |
| [![](https://gitlab.com/bo-art-of-bonsai/archived/automates/openapi-codegen/-/jobs/artifacts/development/raw/python.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/automates/openapi-codegen) | [![](https://gitlab.com/bo-art-of-bonsai/archived/automates/openapi-codegen/-/jobs/artifacts/master/raw/python.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/automates/openapi-codegen) |
| [![](https://gitlab.com/bo-art-of-bonsai/archived/automates/openapi-codegen/-/jobs/artifacts/development/raw/bolibrary.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/automates/openapi-codegen) | [![](https://gitlab.com/bo-art-of-bonsai/archived/automates/openapi-codegen/-/jobs/artifacts/master/raw/bolibrary.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/automates/openapi-codegen) |
| [![](https://gitlab.com/bo-art-of-bonsai/archived/automates/openapi-codegen/-/jobs/artifacts/development/raw/commits.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/automates/openapi-codegen) | [![](https://gitlab.com/bo-art-of-bonsai/archived/automates/openapi-codegen/-/jobs/artifacts/master/raw/commits.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/automates/openapi-codegen) |
| [![](https://gitlab.com/bo-art-of-bonsai/archived/automates/openapi-codegen/-/jobs/artifacts/development/raw/last_commit.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/automates/openapi-codegen) | [![](https://gitlab.com/bo-art-of-bonsai/archived/automates/openapi-codegen/-/jobs/artifacts/master/raw/last_commit.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/automates/openapi-codegen) |
| [![](https://gitlab.com/bo-art-of-bonsai/archived/automates/openapi-codegen/-/jobs/artifacts/development/raw/pylint.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/automates/openapi-codegen/-/jobs) | |
| [![](https://gitlab.com/bo-art-of-bonsai/archived/automates/openapi-codegen/badges/development/coverage.svg)](https://gitlab.com/bo-art-of-bonsai/archived/automates/openapi-codegen/-/jobs) | |
 

#### <a href='https://gitlab.com/bo-art-of-bonsai/archived/doc'><img src='images/doc.png' alt='doc' width='32px'/>doc</a>

- deploy_type: markdown
- editor: BO
- coverage: False
- description: Documentation related to the infra and App
- technology: markdown, json, xml, png
- repository: https://gitlab.com/bo-art-of-bonsai/archived/doc
- environment: backend
- security: no
- url: 
- doc: 
 

| Delivery                    | Deployment                  |
| :-------------------------- | :-------------------------- |
| [![](https://gitlab.com/bo-art-of-bonsai/archived/doc/badges/development/pipeline.svg)](https://gitlab.com/bo-art-of-bonsai/archived/doc/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/archived/doc/badges/master/pipeline.svg)](https://gitlab.com/bo-art-of-bonsai/archived/doc/pipelines) |
| [![](https://gitlab.com/bo-art-of-bonsai/archived/doc/-/jobs/artifacts/development/raw/version.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/doc/pipelines) | | [![](https://gitlab.com/bo-art-of-bonsai/archived/doc/-/jobs/artifacts/master/raw/development.svg?job=deploy%20markdown%20on%20dev)](https://gitlab.com/bo-art-of-bonsai/archived/doc/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/archived/doc/-/jobs/artifacts/master/raw/integration.svg?job=deploy%20markdown%20on%20int)](https://gitlab.com/bo-art-of-bonsai/archived/doc/pipelines) | [![](https://gitlab.com/bo-art-of-bonsai/archived/doc/-/jobs/artifacts/master/raw/production.svg?job=deploy%20markdown%20on%20prod)](https://gitlab.com/bo-art-of-bonsai/archived/doc/pipelines) |
| [![](https://gitlab.com/bo-art-of-bonsai/archived/doc/-/jobs/artifacts/development/raw/python.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/doc) | [![](https://gitlab.com/bo-art-of-bonsai/archived/doc/-/jobs/artifacts/master/raw/python.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/doc) |
| [![](https://gitlab.com/bo-art-of-bonsai/archived/doc/-/jobs/artifacts/development/raw/bolibrary.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/doc) | [![](https://gitlab.com/bo-art-of-bonsai/archived/doc/-/jobs/artifacts/master/raw/bolibrary.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/doc) |
| [![](https://gitlab.com/bo-art-of-bonsai/archived/doc/-/jobs/artifacts/development/raw/commits.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/doc) | [![](https://gitlab.com/bo-art-of-bonsai/archived/doc/-/jobs/artifacts/master/raw/commits.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/doc) |
| [![](https://gitlab.com/bo-art-of-bonsai/archived/doc/-/jobs/artifacts/development/raw/last_commit.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/doc) | [![](https://gitlab.com/bo-art-of-bonsai/archived/doc/-/jobs/artifacts/master/raw/last_commit.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/doc) |
| [![](https://gitlab.com/bo-art-of-bonsai/archived/doc/-/jobs/artifacts/development/raw/pylint.svg?job=badges%20setup)](https://gitlab.com/bo-art-of-bonsai/archived/doc/-/jobs) | |
| [![](https://gitlab.com/bo-art-of-bonsai/archived/doc/badges/development/coverage.svg)](https://gitlab.com/bo-art-of-bonsai/archived/doc/-/jobs) | |
 



Enjoy :)