# Doc

Continuous Delivery
[![](https://gitlab.com/bo-art-of-bonsai/snitch/badges/master/pipeline.svg)](https://gitlab.com/bo-art-of-bonsai/snitch/pipelines)
[![](https://gitlab.com/bo-art-of-bonsai/snitch/-/jobs/artifacts/master/raw/development.svg?job=deploy%20serverless%20on%20dev)]()
[![](https://gitlab.com/bo-art-of-bonsai/snitch/-/jobs/artifacts/master/raw/integration.svg?job=deploy%20serverless%20on%20int)]()
[![](https://gitlab.com/bo-art-of-bonsai/snitch/-/jobs/artifacts/master/raw/production.svg?job=deploy%20serverless%20on%20prod)]()

All necessary information to understand, manage and configure BO's infra and its applications.

## Summary

- [Application list](./doc/APPLICATIONS.md)
- [Status of the deployment](./doc/DASHBOARD.md)

## Presentation

- [IT presentation](https://bonsaibo-my.sharepoint.com/:p:/g/personal/guillain_sanchez_bonsai_bo/EWufsLxwMONIhTWUmYh1RToBhRnVWnnBcDgddKzzVbtlWQ?e=kSkStB)
- [App presentation](https://bonsaibo-my.sharepoint.com/:p:/g/personal/guillain_sanchez_bonsai_bo/EU58zZ6O19BIvpgemTM4oQUBgfOkD7q8EP95cVeGxoOwlQ?e=9OVSOt)
- [Notifications](https://bonsaibo-my.sharepoint.com/:p:/g/personal/guillain_sanchez_bonsai_bo/EQpd0T5f5XFNifkXwh2_rGgB9iTl3GX_oRFIy2c7hqWfkQ?e=euUkaG)
- [IT delivery](https://bonsaibo-my.sharepoint.com/:p:/g/personal/guillain_sanchez_bonsai_bo/EdmDQaI5eSVMiXKwVEnmXugB7hSTV5s_HyG13FRqCr5UfQ?e=Pa9MFC)

## IoT

### Frame

- [Event IoT frame](./doc/IoT-Frame/event.md)
- [Scheduled IoT frame](./doc/IoT-Frame/scheduled.md)

## OpenAPI

### Art-I

- [UML](./doc/arti.uml)
- [JSON](./doc/arti.json)

### Blocker

- [UML](./doc/blocker.uml)
- [JSON](./doc/blocker.json)

### Watcher

- [UML](./doc/watcher.uml)
- [JSON](./doc/watcher.json)

Enjoy :)