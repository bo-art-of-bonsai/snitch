""" Main script to orchestrate the execution """
import os
import urllib.request

from applications import applications
from dashboard import dashboard
from swagger_to_puml import swagger_to_puml

REPORT_FOLDER = "doc"
ARTI_OPENAPI_URL = "https://arti.bonsaibo.com/openapi.json"
BLOCKER_OPENAPI_URL = "https://blocker.bonsaibo.com/openapi.json"
WATCHER_OPENAPI_URL = "https://watcher.bonsaibo.com/openapi.json"


def create_markdown(name: str, fct) -> None:
    """
    Use to create a markdown file
    :param name: Markdown file name
    :param fct: Function used to fulfil the file
    :return: None
    """
    print("Create markdown: ", name.title())
    file = open(os.path.join(REPORT_FOLDER, f"{name.upper()}.md"), "w")
    file.write("\n".join(fct))
    file.close()


def create_diagram(name: str, url: str) -> None:
    """
    Use to create UML diagram from openAPI v3 JSON doc coming from the API.
    :param name: Diagram name
    :param url: API url to get the openAPI v3 JSON doc
    :return: None
    """
    print("Create diagram: ", name.title())

    json_file = os.path.join(REPORT_FOLDER, f"{name.title()}.json")
    uml_file = os.path.join(REPORT_FOLDER, f"{name.title()}.uml")

    urllib.request.urlretrieve(url, json_file)  # nosec
    file = open(uml_file, "w")
    file.write(swagger_to_puml(json_file))
    file.close()


def main() -> bool:
    """
    Main function
    :return: None
    """
    if not os.path.exists(REPORT_FOLDER):
        os.makedirs(REPORT_FOLDER)

    create_markdown("dashboard", dashboard())
    create_markdown("applications", applications())

    create_diagram("arti", ARTI_OPENAPI_URL)
    create_diagram("blocker", BLOCKER_OPENAPI_URL)
    create_diagram("watcher", WATCHER_OPENAPI_URL)

    return True


if __name__ == "__main__":
    main()
