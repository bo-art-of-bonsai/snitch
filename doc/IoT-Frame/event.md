# Event trame

## Prototype
- Octet   Type        Description
- 0       uint8
  -       Bit 0..5:   RFU
  -       Bit 6..7:   ID de trame (01 pour FRAME_GPS) 
- 1..4    float       Latitude (en deg)
- 5..8    float       Longitude (en deg)
- 9       uint8       Tension batterie
- 10..11  uint8[2]    Attitude

## Exemple
Frame : 40 78 77 43 42 54 55 15 40 D7 01 00 contient :
- Latitude : 48.8667° (little-endian)
- Longitude : 2.33333° (little-endian)
- Tension batterie : 4.2V
- Firmware : v1.0