FROM tiangolo/uvicorn-gunicorn-fastapi:python3.9-alpine3.14
LABEL maintainer="guillain@gmail.com"

ARG VERSION=''
ARG BOLIBRARY_VERSION=''
ARG REGISTRY_TOKEN=''

ENV VERSION=${VERSION}

COPY requirements.txt /tmp/
RUN pip install --no-cache-dir -r /tmp/requirements.txt --upgrade && rm -f /tmp/requirements.txt

COPY ./app /app
WORKDIR /app

CMD ["python3", "main.py"]