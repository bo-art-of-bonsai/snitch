# Scheduled frame

## Prototype
- Octet   Type        Description
- 0      uint8       Octet de statut :
  -      Bit 0       RESET pressé durant la dernière heure
  -      Bit 1       Seuil de détection accéléro 1 dépassé
  -      Bit 2       Seuil de détection accéléro 2 dépassé
  -      Bit 3       Seuil de détection accéléro 3 dépassé
  -      Bit 4       Seuil de détection accéléro 4 dépassé
  -      Bit 5       RFU
  -      Bit 6..7    ID de trame (00 pour FRAME_MEASURE)
- 1      int8        Luminosité1 – Moyenne - L(lux) = 10 ^ (V / 50)
- 2      int8        Luminosité1 – Ecart-type
- 3      int8        Luminosité2 – Moyenne
- 4      int8        Luminosité2 – Ecart-type
- 5..7   uint24 / float
  -           12 premiers bits : T(°C) (= V / 8 – 40)
  -           12 derniers bits : hygrométrie (= 1 + (V x 1.5 / 1024))


## Example
Frame : 09 96 64 E0 B9 1F 40 FA 00 80 80 00
- L1m = 1000 lux
- L1e = 100 lux
- L2m = 30000 lux // 30199
- L2e = 5000 lux // 5011
- T = 22.5°C
- H = 1.37
- Status : RESET et seuil de détection 3 dépassés