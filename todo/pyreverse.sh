for app_dir in `find /d/documents/GitLab/bo-art-of-bonsai/infra/source/ -name app | grep -v '.git'`; do
    for file in `find ${app_dir} -name *.py | grep -v bolib`; do`
    init_done=0
    if [ ! -f "${app_dir}/__init__.py" ]; then
        touch "${app_dir}/__init__.py"
        init_done=1
    fi
    pyreverse ${app_dir}
    if [ ${init_done} -eq 1 ]; then
        rm -f "${app_dir}/__init__.py"
    fi
    mv classes.dot "doc/`basename ${app_dir}`.dot"
done
