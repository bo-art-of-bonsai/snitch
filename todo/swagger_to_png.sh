#!/bin/bash

IN_FILE=./openapi.yaml
GEN_FILE_PUML=./openapi.puml

# Swagger --> Plant UML
pip3 install -r requirements.txt > /dev/null
python3 swagger_to_puml.py ${IN_FILE} > ${GEN_FILE_PUML}

# Plant UML --> PNG
# docker build -t plantuml ./plantuml
./plantuml/run-plantuml  -tpng -charset utf-8 $GEN_FILE_PUML

exit 0
