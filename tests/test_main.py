import os
import sys
import unittest

sys.path.append(os.getcwd())
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), "..", "app"))

from app.main import main, create_markdown, create_diagram


class TestMain(unittest.TestCase):
    def test_main(self):
        self.assertTrue(main())

    def test_create_markdown(self):
        create_markdown("test", "test")

    def test_create_diagram(self):
        create_diagram("test", "https://arti.bonsaibo.com/openapi.json")


if __name__ == "__main__":
    unittest.main()
