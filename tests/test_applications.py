import os
import sys
import unittest

sys.path.append(os.getcwd())
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), ".."))

from app.applications import applications


class TestApplications(unittest.TestCase):
    def test_applications(self):
        self.assertTrue(len(applications()) > 0)


if __name__ == "__main__":
    unittest.main()
