""" Script to generate the dashboard of all application based on the application.json definition """
import json
import os

# Markdown templates definition
TBL_HEADER_CPT = (
    "| Component  | deploy_type   | Technology | Delivery     | Deployment    | Deployed |"
)
TBL_HEADER_LINE = (
    "| :--------- | :---------| :--------- | :----------: | :-----------: | :------: |"
)
MD_HEADER = """# Dashboard

## Branches

- **Development**: code management and branch release
- **Master**: 
    - release manager: 
        - tag/package/library: patch/fix, minor (X.Y.Z), major (X.Y)
        - container: build, latest, stable
    - deployment manager: 
        - target: on EC2 container and Lambda fct
        - environment: Development -> Integration -> Production

## Applications
"""
MD_FOOTER = """

Enjoy :)"""

JSON_CONF_FILE = os.path.join(
    os.path.dirname(os.path.realpath(__file__)), "applications.json"
)

# Load JSON file conf
with open(JSON_CONF_FILE) as json_file:
    definition = json.load(json_file)


def dashboard() -> list:
    """
    Generate dashboard report
    :return: Report under list format
    """
    applications = {}
    for line in definition:
        applications[line["application"]] = line

    chapters = []
    for line in definition:
        if line["type"] not in chapters:
            chapters.append(line["type"])

    report = [MD_HEADER]
    for chapter in chapters:
        report.append(f"### {str(chapter).title()}")
        report.append(TBL_HEADER_CPT)
        report.append(TBL_HEADER_LINE)

        for app_key, app_val in applications.items():
            if app_val["type"] != chapter:
                continue

            url = app_val["repository"]
            image = f"<a href='{url}'><img src='images/{app_key}.png' alt='{app_key}' width='32px'/>{app_key}</a>"
            line = [image, app_val["deploy_type"], app_val["technology"]]

            for branch in ["development", "master"]:
                msg_txt = (
                    f"[![]({url}/badges/{branch}/pipeline.svg)]({url}/pipelines) "
                    f"[![]({url}/-/jobs/artifacts/{branch}/raw/python.svg?job=badges%20setup)]({url}/container_registry) "
                    f"[![]({url}/-/jobs/artifacts/{branch}/raw/version.svg?job=badges%20setup)]({url}/container_registry) "
                    f"[![]({url}/-/jobs/artifacts/{branch}/raw/bolibrary.svg?job=badges%20setup)]({url}/container_registry) "
                    f"[![]({url}/-/jobs/artifacts/{branch}/raw/last_commit.svg?job=badges%20setup)]({url}/-/commits/{branch}) "
                    f"[![]({url}/-/jobs/artifacts/{branch}/raw/commits.svg?job=badges%20setup)]({url}/-/commits/{branch})"
                )

                if branch == "development" and "python" in app_val["technology"]:
                    msg_txt += f"[![]({url}/badges/{branch}/coverage.svg)]({url}/-/jobs)"
                    msg_txt += f"[![]({url}/-/jobs/artifacts/{branch}/raw/pylint.svg?job=badges%20setup)]({url})"

                line.append(msg_txt)

            msg_txt = ""
            my_env = {"development": "dev", "integration": "int", "production": "prod"}
            for env in my_env:
                job = f"deploy%20{app_val['deploy_type']}%20on%20{my_env[env]}"
                msg_txt += f"[![]({url}/-/jobs/artifacts/master/raw/{env}.svg?job={job})]({url}) "
            line.append(msg_txt)

            report.append(f"|{'|'.join(line)}|")
    report.append(MD_FOOTER)
    return report


if __name__ == "__main__":
    print("\n".join(dashboard()))
