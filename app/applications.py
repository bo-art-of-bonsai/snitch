""" Use to generate report based on the application.json file definition """
import json
import os

# Markdown templates definition
MD_HEADER = """# Applications

## Branches

- **Development**: code management and branch release
- **Master**: 
    - release manager: 
        - tag/package/library: patch/fix, minor (X.Y.Z), major (X.Y)
        - container: build, latest, stable
    - deployment manager: 
        - target: on EC2 container and Lambda fct
        - environment: Development -> Integration -> Production

## Applications
"""
MD_FOOTER = """

Enjoy :)"""

JSON_CONF_FILE = os.path.join(
    os.path.dirname(os.path.realpath(__file__)), "applications.json"
)

# Load JSON file conf
with open(JSON_CONF_FILE) as json_file:
    definition = json.load(json_file)


def create_app_summary(app_val: list) -> str:
    """
    Creat application summary with the help of the application.json file (coming from app_val)
    :param app_val: doc of the app
    :return: markdown bullet points as str
    """
    line = []
    for val in app_val:
        if val in ["type", "application"]:
            continue
        line.append(f"{val}: {app_val[val]}")
    return "\n- ".join(line) + "\n"


def create_badge_report(app_val: dict) -> str:
    """
    Create a markdown array which contain the badges as reporters
    :param app_val: doc of the app
    :return: markdown array as str
    """
    url = app_val["repository"]
    job = f"deploy%20{app_val['deploy_type']}%20on%20"
    line = ["| Delivery                    | Deployment                  |",
            "| :-------------------------- | :-------------------------- |",
            f"| [![]({url}/badges/development/pipeline.svg)]({url}/pipelines) "
            f"| [![]({url}/badges/master/pipeline.svg)]({url}/pipelines) |"]
    line.append(f"| [![]({url}/-/jobs/artifacts/development/raw/version.svg?job=badges%20setup)]({url}/pipelines) | "
                f"| [![]({url}/-/jobs/artifacts/master/raw/development.svg?job={job}dev)]({url}/pipelines) "
                f"| [![]({url}/-/jobs/artifacts/master/raw/integration.svg?job={job}int)]({url}/pipelines) "
                f"| [![]({url}/-/jobs/artifacts/master/raw/production.svg?job={job}prod)]({url}/pipelines) |")
    for badge in ("python", "bolibrary", "commits", "last_commit"):
        txt = "|"
        for branch in ["development", "master"]:
            txt += f" [![]({url}/-/jobs/artifacts/{branch}/raw/{badge}.svg?job=badges%20setup)]({url}) |"
        line.append(txt)
    line.append(f"| [![]({url}/-/jobs/artifacts/development/raw/pylint.svg?job=badges%20setup)]({url}/-/jobs) | |")
    line.append(f"| [![]({url}/badges/development/coverage.svg)]({url}/-/jobs) | |")
    return "\n".join(line) + "\n"


def applications() -> list:
    """
    Generate application report
    :return: Report under list format
    """
    report = []
    apps = {}
    for line in definition:
        apps[line["application"]] = line

    chapters = []
    for line in definition:
        if line["type"] not in chapters:
            chapters.append(line["type"])

    report.append(MD_HEADER)
    for chapter in chapters:
        report.append(f"### {str(chapter).title()}")

        for app_key, app_val in apps.items():
            if app_val["type"] != chapter:
                continue

            url = app_val["repository"]
            image = f"<a href='{url}'><img src='images/{app_key}.png' alt='{app_key}' width='32px'/>{app_key}</a>"

            report.append(f"#### {image}\n")
            report.append(f"- {create_app_summary(app_val)} \n")
            report.append(f"{create_badge_report(app_val)} \n")

    report.append(MD_FOOTER)

    return report


if __name__ == "__main__":
    print("\n".join(applications()))
