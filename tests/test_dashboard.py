import os
import sys
import unittest

sys.path.append(os.getcwd())
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), ".."))

from app.dashboard import dashboard


class TestDashboard(unittest.TestCase):
    def test_dashboard(self):
        self.assertTrue(len(dashboard()) > 0)


if __name__ == "__main__":
    unittest.main()
